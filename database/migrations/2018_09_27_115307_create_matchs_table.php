<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchs', function (Blueprint $table) {
            $table->increments('id');
            // Lien avec l'id compétition
            $table->integer('competition_id')->unsigned();
            $table->foreign('competition_id')->references('id')->on('competitions')->onDelete('cascade');
            // Equipe à domicile ID
            $table->integer('team_dom_id')->unsigned();
            $table->foreign('team_dom_id')->references('id')->on('teams')->onDelete('cascade');
            // Equipe à l'extérieur ID
            $table->integer('team_ext_id')->unsigned();
            $table->foreign('team_ext_id')->references('id')->on('teams')->onDelete('cascade');
            // Stade dans lequel va se jouer le match
            $table->integer('stadium_id')->unsigned();
            $table->foreign('stadium_id')->references('id')->on('matchs')->onDelete('cascade');
            // Journée du match : En string pour pouvoir mettre "Quart de finale" ...
            $table->string('journee')->default('');
            // Date du match
            $table->dateTime('date');
            // Match passé ?
            $table->boolean('archived')->default(false);
            // Match mis en avant ?
            $table->boolean('highlighted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matchs');
    }
}
