<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city_start');
            $table->decimal('lat_city_start', 10,8);
            $table->decimal('lng_city_start', 11,8);
            $table->datetime('date_depart');
            $table->integer('total_nb_places');
            $table->integer('remaining_places');
            $table->float('unit_price');
            $table->integer('distance');
            $table->string('duration');
            $table->boolean('going_coming');
            $table->boolean('return_after_match');
            $table->datetime('date_return');
            $table->timestamps();
            $table->integer('match_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('match_id')->references('id')->on('matchs')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
