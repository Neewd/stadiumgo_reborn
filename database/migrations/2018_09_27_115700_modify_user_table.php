<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // Add new column 
            $table->string('activation_code')->nullable();
            $table->boolean('status')->default(0);
            $table->string('firstname');
            $table->string('lastname');
            $table->date('birthdate')->nullable();
            $table->text('description')->nullable();
            $table->string('vehicle')->nullable();
            $table->string('avatar_path')->default('default.png');
            $table->integer('id_team_supported')->unsigned()->nullable();
            $table->foreign('id_team_supported')->references('id')->on('teams');
            $table->float('average_note')->nullable();
            $table->string('role')->default('member');

            // Remove unused column
            $table->dropColumn('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            // Remove column
            $table->dropColumn('activation_code')->nullable();
            $table->dropColumn('status');
            $table->dropColumn('firstname');
            $table->dropColumn('lastname');
            $table->dropColumn('birthdate')->nullable();
            $table->dropColumn('description')->nullable();
            $table->dropColumn('vehicle')->nullable();
            $table->dropColumn('avatar_path');
            $table->dropColumn('id_team_supported');
            $table->dropColumn('average_note');
            $table->dropColumn('role');

            // Add the removed columns
            $table->string('name');
        });
    }
}
