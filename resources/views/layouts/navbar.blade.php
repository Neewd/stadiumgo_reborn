<header id="m_header" class="m-grid__item m-header " m-minimize="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200" style="box-shadow : none !important;">
    @if (\Request::is('/')) 
    <div class="m-header__top grey">
    @else
    <div class="m-header__top">
    @endif
    <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">
            <!-- begin::Brand -->
            @if (\Request::is('/')) 
            <div class="m-stack__item m-brand grey">
                @else
                <div class="m-stack__item m-brand">
                    @endif
                    <div class="m-stack m-stack--ver m-stack--general m-stack--inline">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                            <a class="navbar-brand" href="/">
                                <svg style="height: 50px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 43" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <defs>
                                        <path id="a" d="M22 2.5c4-3.8 6.7-2.4 6 3.2l-1.5 10h4.2c5.5 0 6.7 3.2 2.6 7L14 40.7c-4 3.7-6.7 2.3-6-3.3l1.5-10H5.3c-5.5 0-6.7-3-2.6-7L22 2.4z"/>
                                        <linearGradient id="b" x1="59.1%" x2="88.7%" y1="55.6%" y2="100%">
                                            <stop stop-color="#F1C476" offset="0%"/>
                                            <stop stop-color="#CC973B" offset="100%"/>
                                        </linearGradient>
                                        <linearGradient id="d" x1="11.3%" x2="40.9%" y1="0%" y2="44.4%">
                                            <stop stop-color="#CC973B" offset="0%"/>
                                            <stop stop-color="#F1C476" offset="100%"/>
                                        </linearGradient>
                                    </defs>
                                    <g fill="none" fill-rule="evenodd">
                                        <path fill="#F1C476" d="M16 8.4c7.3-7 12.3-4.4 10.8 5.7l-.2 2c7.8 0 8 5.7.6 12.7l-7 6.5c-7.5 7-12.4 4.5-11-5.7l.3-1.8c-7.8 0-8-5.7-.7-12.6l7-6.5z"/>
                                        <g transform="translate(.037 .147)">
                                            <mask id="c" fill="#fff">
                                                <use xlink:href="#a"/>
                                            </mask>
                                            <use fill="#F1C476" xlink:href="#a"/>
                                            <path fill="url(#b)" d="M3.8-1.5h25.6v17.3H3.8" mask="url(#c)"/>
                                            <path fill="url(#d)" d="M6.6 27.3h25.6v17.3H6.6z" mask="url(#c)"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- end::Brand -->   
                <!-- begin::Topbar -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline d-flex">
                                <li class="m-nav__item d-flex align-items-center">
                                    <a href="{{ route('proposer-trajet') }}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder" style="margin-right : 5px;">Proposer un trajet</a>
                                </li>
                                @auth
                                <li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center  m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                    <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                                    <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
                                    <span class="m-nav__link-icon">
                                    <span class="m-nav__link-icon-wrapper">
                                    <i class="flaticon-alarm"></i>
                                    </span>
                                    </span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(./assets/app/media/img/misc/notification_bg.jpg); background-size: cover;">
                                                <span class="m-dropdown__header-title">9 New</span>
                                                <span class="m-dropdown__header-subtitle">User Notifications</span>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
                                                        <li class="nav-item m-tabs__item">
                                                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">
                                                            Alerts
                                                            </a>
                                                        </li>
                                                        <li class="nav-item m-tabs__item">
                                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_events" role="tab">Events</a>
                                                        </li>
                                                        <li class="nav-item m-tabs__item">
                                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_logs" role="tab">Logs</a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
                                                            <div class="m-scrollable m-scroller ps" data-scrollable="true" data-height="250" data-mobile-height="200" style="height: 250px; overflow: hidden;">
                                                                <div class="m-list-timeline m-list-timeline--skin-light">
                                                                    <div class="m-list-timeline__items">
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                                                            <span class="m-list-timeline__text">12 new users registered</span>
                                                                            <span class="m-list-timeline__time">Just now</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge"></span>
                                                                            <span class="m-list-timeline__text">System shutdown <span class="m-badge m-badge--success m-badge--wide">pending</span></span>
                                                                            <span class="m-list-timeline__time">14 mins</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge"></span>
                                                                            <span class="m-list-timeline__text">New invoice received</span>
                                                                            <span class="m-list-timeline__time">20 mins</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge"></span>
                                                                            <span class="m-list-timeline__text">DB overloaded 80% <span class="m-badge m-badge--info m-badge--wide">settled</span></span>
                                                                            <span class="m-list-timeline__time">1 hr</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge"></span>
                                                                            <span class="m-list-timeline__text">System error - <a href="#" class="m-link">Check</a></span>
                                                                            <span class="m-list-timeline__time">2 hrs</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item m-list-timeline__item--read">
                                                                            <span class="m-list-timeline__badge"></span>
                                                                            <span href="" class="m-list-timeline__text">New order received <span class="m-badge m-badge--danger m-badge--wide">urgent</span></span>
                                                                            <span class="m-list-timeline__time">7 hrs</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item m-list-timeline__item--read">
                                                                            <span class="m-list-timeline__badge"></span>
                                                                            <span class="m-list-timeline__text">Production server down</span>
                                                                            <span class="m-list-timeline__time">3 hrs</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge"></span>
                                                                            <span class="m-list-timeline__text">Production server up</span>
                                                                            <span class="m-list-timeline__time">5 hrs</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                                                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                                                </div>
                                                                <div class="ps__rail-y" style="top: 0px; right: 4px;">
                                                                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
                                                            <div class="m-scrollable m-scroller ps" data-scrollable="true" data-height="250" data-mobile-height="200" style="height: 250px; overflow: hidden;">
                                                                <div class="m-list-timeline m-list-timeline--skin-light">
                                                                    <div class="m-list-timeline__items">
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                                                            <a href="" class="m-list-timeline__text">New order received</a>
                                                                            <span class="m-list-timeline__time">Just now</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>
                                                                            <a href="" class="m-list-timeline__text">New invoice received</a>
                                                                            <span class="m-list-timeline__time">20 mins</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                                                            <a href="" class="m-list-timeline__text">Production server up</a>
                                                                            <span class="m-list-timeline__time">5 hrs</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                            <a href="" class="m-list-timeline__text">New order received</a>
                                                                            <span class="m-list-timeline__time">7 hrs</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                            <a href="" class="m-list-timeline__text">System shutdown</a>
                                                                            <span class="m-list-timeline__time">11 mins</span>
                                                                        </div>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                            <a href="" class="m-list-timeline__text">Production server down</a>
                                                                            <span class="m-list-timeline__time">3 hrs</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                                                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                                                </div>
                                                                <div class="ps__rail-y" style="top: 0px; right: 4px;">
                                                                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                                                            <div class="m-stack m-stack--ver m-stack--general" style="min-height: 180px;">
                                                                <div class="m-stack__item m-stack__item--center m-stack__item--middle">
                                                                    <span class="">All caught up!<br>No new logs.</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                    <span class="m-topbar__welcome">Bonjour,&nbsp;</span>  
                                    <span class="m-topbar__username">{{ Auth::user()->firstname }}</span> 
                                    <span class="m-topbar__userpic">
                                        <img src="images/users/{{ Auth::user()->avatar_path }}" class="m--img-rounded m--marginless m--img-centered" alt="">
                                    </span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(./assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                                                <div class="m-card-user m-card-user--skin-dark">
                                                    <div class="m-card-user__pic">
                                                        <img src="#" class="m--img-rounded m--marginless" alt="">
                                                    </div>
                                                    <div class="m-card-user__details">
                                                        <span class="m-card-user__name m--font-weight-500">{{ Auth::user()->lastname }} {{ Auth::user()->firstname }}</span>
                                                        <a href="" class="m-card-user__email m--font-weight-300 m-link">{{ Auth::user()->email }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">
                                                        <li class="m-nav__section m--hide">
                                                            <span class="m-nav__section-text">Section</span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="/my-account" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                            <span class="m-nav__link-title">  
                                                            <span class="m-nav__link-wrap">      
                                                            <span class="m-nav__link-text">Mon compte</span>      
                                                            <span class="m-nav__link-badge"><span class="m-badge m-badge--success">2</span></span>  
                                                            </span>
                                                            </span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="?page=profile&amp;demo=demo5" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                            <span class="m-nav__link-text">Messages</span>
                                                            </a>
                                                        </li>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="?page=profile&amp;demo=demo5" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-info"></i>
                                                            <span class="m-nav__link-text">FAQ</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="?page=profile&amp;demo=demo5" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                            <span class="m-nav__link-text">Support</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__separator m-nav__separator--fit">
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="{{ route('logout') }}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder col-12"
                                                                onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();">
                                                            Se déconnecter
                                                            </a>
                                                        </li>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li id="m_quick_sidebar_toggle" class="m-nav__item">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                    <span class="m-nav__link-icon m-nav__link-icon--aside-toggle">
                                    <span class="m-nav__link-icon-wrapper"><i class="flaticon-menu-button"></i></span>
                                    </span>
                                    </a>
                                </li>
                                @else 
                                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light">
                                    <a href="{{ route('register') }}" class="m-nav__link m-dropdown__toggle">
                                    <span class="m-topbar__welcome" style="font-size: 1.2rem !important; color:#2d3436;">{{ __('S\'inscrire') }}</span>
                                    </a>
                                </li>
                                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light">
                                    <a href="{{ route('login') }}" class="m-nav__link m-dropdown__toggle">
                                    <span class="m-topbar__welcome" style="font-size: 1.2rem !important; color: #2d3436;">{{ __('Se connecter') }}</span>
                                    </a>
                                </li>
                                @endauth
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- end::Topbar -->            
            </div>
        </div>
    </div>
    @if (\Request::is('dashboard') || \Request::is('dashboard/*') )  
    <div class="m-header__bottom">
        <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <!-- begin::Horizontal Menu -->
                <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
                    <button class="m-aside-header-menu-mobile-close m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn">
                    <i class="la la-close"></i>
                    </button>
                    <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
                        <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                            <li class="m-menu__item  m-menu__item--active " aria-haspopup="true">
                                <a href="/dashboard" class="m-menu__link ">
                                    <span class="m-menu__item-here"></span>
                                    <span class="m-menu__link-text">Dashboard</span>
                                </a>
                            </li>
                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" aria-haspopup="true">
                                <a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link">
                                    <span class="m-menu__item-here"></span>
                                    <span class="m-menu__link-text">Settings</span>
                                    <i class="m-menu__hor-arrow la la-angle-down"></i>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
                                    <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item" aria-haspopup="true">
                                            <a href="/dashboard/settings/api" class="m-menu__link ">
                                                <i class="m-menu__link-icon flaticon-file"></i>
                                                <span class="m-menu__link-text">API</span>
                                            </a>
                                        </li>
                                        <li class="m-menu__item  m-menu__item--submenu" m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link"><i class="m-menu__link-icon flaticon-chat-1"></i><span class="m-menu__link-text">Exemple avec Dropdown</span><i class="m-menu__hor-arrow la la-angle-right"></i><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
                                                <span class="m-menu__arrow "></span>
                                                <ul class="m-menu__subnav">
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="?page=inner&amp;demo=demo5" class="m-menu__link "><span class="m-menu__link-text">Customer Feedbacks</span></a></li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="?page=inner&amp;demo=demo5" class="m-menu__link "><span class="m-menu__link-text">Supplier Feedbacks</span></a></li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="?page=inner&amp;demo=demo5" class="m-menu__link "><span class="m-menu__link-text">Reviewed Feedbacks</span></a></li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="?page=inner&amp;demo=demo5" class="m-menu__link "><span class="m-menu__link-text">Resolved Feedbacks</span></a></li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="?page=inner&amp;demo=demo5" class="m-menu__link "><span class="m-menu__link-text">Feedback Reports</span></a></li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- end::Horizontal Menu -->   
                <!--begin::Search-->
                <div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-" id="m_quicksearch" m-quicksearch-mode="default">
                    <!--begin::Search Form -->
                    <form class="m-header-search__form">
                        <div class="m-header-search__wrapper">
                            <span class="m-header-search__icon-search" id="m_quicksearch_search">
                            <i class="la la-search"></i>
                            </span>
                            <span class="m-header-search__input-wrapper">
                            <input autocomplete="off" type="text" name="q" class="m-header-search__input" value="" placeholder="Search..." id="m_quicksearch_input">
                            </span>
                            <span class="m-header-search__icon-close" id="m_quicksearch_close">
                            <i class="la la-remove"></i> 
                            </span>
                            <span class="m-header-search__icon-cancel" id="m_quicksearch_cancel">
                            <i class="la la-remove"></i>
                            </span>
                        </div>
                    </form>
                    <!--end::Search Form -->
                    <!--begin::Search Results -->
                    <div class="m-dropdown__wrapper" style="z-index: 101;">
                        <div class="m-dropdown__arrow m-dropdown__arrow--center"></div>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__scrollable m-scrollable m-scroller ps" data-scrollable="true" data-height="300" data-mobile-height="200" style="height: 300px; overflow: hidden;">
                                    <div class="m-dropdown__content m-list-search m-list-search--skin-light">
                                    </div>
                                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                    </div>
                                    <div class="ps__rail-y" style="top: 0px; right: 4px;">
                                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Search Results -->
                </div>
                <!--end::Search-->          
            </div>
        </div>
    </div>
    @endif
</header>