<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'StadiumGO') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto:300,400,500,600,700" media="all">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/perfect-scrollbar.min.js') }}" defer></script>
    <script src="{{ asset('js/scripts.bundle.js') }} " type="text/javascript" defer></script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOHq-KA_GN-WAieT9BmPbUM5fMr5le2Fc&libraries=places"></script>
    
</head>
<body>
    <div id="app">
        @if (\Request::is('/'))  
            <div class="angles grey"></div>
        @endif


            @include('layouts.navbar')


        <main class="py-4">

            @yield('content')
        </main>
    </div>
</body>
</html>
