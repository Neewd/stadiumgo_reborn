@extends('layouts.app')

@section('content')
<div class="container">
    <!-- Settings -->
    <div class="row">
       <div class="col-md-3">
          <div class="panel panel-default panel-flush">
             <div class="panel-heading">
                Settings
             </div>
             <div class="panel-body">
                <div class="spark-settings-tabs">
                   <ul class="nav flex-column">
                      <li class="nav-item">
                         <a class="nav-link active" href="/dashboard/settings/api">API</a>
                      </li>
                   </ul>
                </div>
             </div>
          </div>
       </div>
       <div class="col-md-9">
           @yield('settings.content')
       </div>
    </div>
</div>
@endsection