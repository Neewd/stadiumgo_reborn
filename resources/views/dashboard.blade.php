@extends('layouts.app')

@section('content')
<div class="container">
	<!-- Proposition de trajet -->
	<div class="row justify-content-center">
		<div class="col-md-12">
			@if (Auth::user()->isAdmin())
				Bonjour monsieur l'administrateur
			@else
				Dommage de ne pas être administrateur tu aurais pu voir un tas de trucs. 	
			@endif
		</div>
	</div>
</div>
@endsection
