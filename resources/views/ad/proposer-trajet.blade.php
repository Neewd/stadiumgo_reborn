@extends('layouts.app') @section('content')
<proposer-trajet inline-template>

    <div class="row d-flex" style="justify-content: center;" v-cloak>
        <div class="col-8">
            <div class="m-portlet m-portlet--full-height">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text" style="margin-right: 5px;">
                                {{__('Proposer un trajet')}} 
                            </h3>
                            <h5 class="m-portlet__head-text">
                                > @{{ steps[activeStepIndex] }}
                            </h5>
                        </div>
                    </div>
                </div>

                <div class="tab-content">
                    <div class="tab-pane active">
                        <form method="POST" action="">
                            @csrf
                            <div class="m-portlet__body">
                                
                                <input type="hidden" ref="userConnected" value="{{Auth::user()->id}}"> 


                                <div class="row">
                                    <div class="col">
                                        <!-- Steps -->
                                        <div class="steps">
                                            <ul role="tablist" class="d-flex justify-content-around">
                                                <li role="tab" class="first current" v-bind:class="{ 'checked' : activeStepIndex >= 0, 'current' : activeStepIndex == 0 }">
                                                    <a></a>
                                                </li>
                                                <li role="tab" class="done" v-bind:class="{ 'checked' : activeStepIndex >= 1, 'current' : activeStepIndex == 1 }">
                                                    <a></a>
                                                </li>
                                                <li role="tab" class="done" v-bind:class="{ 'checked' : activeStepIndex >= 2, 'current' : activeStepIndex == 2 }">
                                                    <a></a>
                                                </li>
                                                <li role="tab" class="last done" v-bind:class="{ 'checked' : activeStepIndex >= 3, 'current' : activeStepIndex == 3 }">
                                                    <a></a>
                                                </li>
                                                <li role="tab" class="last done" v-bind:class="{ 'checked' : activeStepIndex >= 4, 'current' : activeStepIndex == 4 }">
                                                    <a></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <!-- Section choisir son club -->
                                <section v-bind:class="{ 'active show' : activeStepIndex == 0 }" class="m-4" style="margin-bottom : 0 !important; display: none;">
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('Club choisi : ') }}</label>

                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="club-choisi" id="club-choisi" ref="club-choisi" required autofocus>
                                        </div>
                                    </div>

                                    <div v-show="clubChoisiNom != '' && matchs.length == 0" style="text-align: center;">
                                        <strong>Aucun match trouvé pour cette équipe.</strong>
                                    </div>
                                    <div v-show="matchs.length >= 0">
                                        <div class="row">
                                            <div class="col-6 col-sm-4" v-for="match in matchs">
                                                <div class="card" v-bind:id="getMatchId(match.id)" v-on:click="selectMatch(match)" v-bind:class=" { 'selected' : (match.id == matchChoisi.id) } ">
                                                    <img class="card-img-top" src="https://bulma.io/images/placeholders/1280x960.png" alt="Card image cap">
                                                    <div class="card-body">
                                                        <p class="card-text">
                                                            @{{ match.homeTeam.name }} - @{{ match.outsideTeam.name }}
                                                            <br>
                                                            <time>@{{ match.date_humans }}</time>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!-- Section ville de départ -->
                                <section v-bind:class="{ 'active show' : activeStepIndex == 1 }" class="m-4" style="margin-bottom : 0 !important; display: none;">
                                    <div class="row">
                                        <div class="col-md-7 d-flex flex-column justify-content-around">

                                            <div class="form-group row">
                                                <label for="matchConcerne" class="col-sm-4 col-form-label">{{ __('Match concerné : ') }}</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-transparent" id="matchConcerne" v-model="matchConcerne" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="villeDepartInput" class="col-sm-4 col-form-label">{{ __('Ville de départ : ') }}</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="villeDepartInput" ref="villeDeDepartInput">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-5">
                                            <div class="card" id="mapFrance" style=" height : 400px; margin-bottom : 0 !important;">
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <!-- Section date départ & heure -->
                                <section v-bind:class="{ 'active show' : activeStepIndex == 2 }" class="m-4" style="margin-bottom : 0 !important; display: none;">
                                    <div class="row">
                                        <div class="col">
                                            Le départ se fera le :
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" placeholder="" id="departDatePicker">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        à 
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="" id="departClockpicker" data-provide="clockpicker" data-autoclose="true">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <div class="input-group mb-3">
                                                Le match commence à      <strong class="m--margin-right-1 m--margin-left-4">@{{ matchChoisi.heure }}</strong>, le trajet aller compte <strong class="m--margin-right-1 m--margin-left-4">@{{ distance }}</strong> km, pour environ <strong class="m--margin-right-1 m--margin-left-4">@{{ tempsTrajet }}.</strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <!-- Aller retour -->
                                            <label class="checkbox">
                                                <input id="switchIsAllerRetour" type="checkbox" name="switchIsAllerRetour" class="switch is-rounded" checked="checked" v-model="isAllerRetour">
                                                <label for="switchIsAllerRetour">Aller-retour</label>
                                            </label>

                                            <!-- Le retour du trajet -->
                                            <div v-if="isAllerRetour">

                                                <div class="field is-horizontal">
                                                    <div class="input-group mb-3" style="margin-right: 15px;">Le retour s'effectuera directement après le match : </div>
                                                    <div class="input-group mb-3">
                                                        <input id="switchDirectAfterMatch" type="checkbox" name="switchDirectAfterMatch" class="switch is-rounded" checked="checked" v-model="returnInfo.directAfterMatch">
                                                        <label for="switchDirectAfterMatch">Le retour s'effectuera directement après le match </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <div v-if="!returnInfo.directAfterMatch">
                                                <div class="field">
                                                    Le retour s'effectuera le :
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="input-group mb-3">
                                                            <input type="text" class="form-control" placeholder="" id="returnDatePicker">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="far fa-calendar"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    à 
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control" placeholder="" id="retourClockPicker">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="far fa-clock"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section v-bind:class="{ 'active show' : activeStepIndex == 3 }" class="m-4" style="margin-bottom: 0 !important; display:  none;">
                                    <div class="row">
                                        <div class="col">
                                            <label for="" class="label">Combien de places disposez-vous : </label>
                                                <input type="number" min="1" class="input" v-model="nbPlaces">
                                        </div>
                                        <div class="col">
                                            <label for="" class="label">Prix par unité : </label>
                                                <input type="number" min="1" class="input" v-model="prixUnite">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col d-flex justify-content-center">
                                            <p>Il y aura <strong>@{{ nbPlaces }}</strong> supporters avec vous, et payeront chacun <strong>@{{ prixUnite }}</strong> euro(s).</p>
                                        </div>
                                    </div>
                                </section>

                                <section v-bind:class="{ 'active show' : activeStepIndex == 4 }" class="m-4" style="margin-bottom: 0 !important; display:  none;">
                                    <div class="row">
                                        <div class="col d-flex justify-content-center">
                                            <h2>@{{ matchChoisi.equipeDomicile.nom }} - @{{ matchChoisi.equipeExterieur.nom }}</h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col m--align-center">
                                            <h4>Trajet</h4>
                                        </div>
                                        <div class="col m--align-center">
                                            <h4>Horaire & Date</h4>
                                        </div>
                                        <div class="col m--align-center">
                                            <h4>Prix & Place</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <p class="m--align-center" v-if="isAllerRetour">Aller retour </p>
                                            <p class="m--align-center" v-else> Aller simple</p>
                                        </div>
                                        <div class="col">
                                            <p class="m--align-center">
                                                Départ <strong>@{{ departInfo.jour}} - @{{ departInfo.heure}}</strong> 
                                            </p>
                                        </div>
                                        <div class="col">
                                            <p class="m--align-center">
                                                <strong>@{{ nbPlaces }} place(s).</strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <p class="m--align-center">
                                                <strong> @{{ departInfo.ville }} </strong> > @{{ matchChoisi.equipeDomicile.stade }}
                                            </p>
                                        </div>
                                        <div class="col">
                                            <p class="m--align-center" v-if="!returnInfo.directAfterMatch">Retour <strong>@{{ returnInfo.jour}} - @{{ returnInfo.heure}}</strong></p>
                                        </div>
                                        <div class="col">
                                            <p class="m--align-center">
                                                <strong>@{{ prixUnite }} € </strong> / supporters
                                            </p>
                                        </div>
                                    </div>
                                </section>

                            </div>

                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-12 d-flex" style="justify-content : space-between;">
                                            <button type="button" class="btn m-btn m-btn--air m-btn--custom" v-on:click.prevent="decrementActiveStepIndex()" v-bind:class=" { 'is-invisible' : activeStepIndex == 0 } ">
                                                Précédent
                                            </button>
                                            <button type="button" class="btn btn-secondary m-btn m-btn--air m-btn--custom" v-bind:class="{ 'd-none hidden' : activeStepIndex >= 4 }" v-bind:disabled="!canWeGoNext" v-on:click.prevent="goToStep(++activeStepIndex)">
                                                Suivant
                                            </button>
                                            <button type="button" class="btn btn-primary m-btn m-btn-air m-btn--custom" v-bind:class="{ 'd-none hidden' : activeStepIndex < 4 }" v-on:click.prevent="registerAd()">
                                                Valider mon annonce !
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</proposer-trajet>
@endsection