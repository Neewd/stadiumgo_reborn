@extends('layouts.app')

@section('content')
<display-annonces-by-search inline-template :ads="{{ json_encode($ads) }}" :ville="{{ json_encode($ville) }}" :club="{{ json_encode($club) }}" v-cloak>
	<div>
		<section class="hero">
			<div class="row d-flex justify-content-center">
				<div class="col-6">
					<div class="row ">
						<div class="col">
							<input id="club-choisi" type="text" name="club-choisi" value="" placeholder="Votre club" required="required" autofocus="autofocus" class="form-control col m--margin-right-5" v-model="searchCriteria.club.name">
						</div>
						<div class="col">
							<input id="villeDepartRecherche" type="text" ref="villeDepartRecherche" name="villeDepartRecherche" value="" placeholder="Votre ville de départ" required="required" autofocus="autofocus" class="form-control col m--margin-left-5" v-model="searchCriteria.ville.name">
						</div>
						<div class="col d-flex justify-content-center">
							<button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air col-8" v-on:click.prevent="getMatchsForSearch()">Rechercher</button>
						</div>
					</div>
				</div>

			</div>
		</section>

		<section class="hero m--margin-top-50">
			<div class="row d-flex justify-content-center">
				<div v-show="dataAds.length == 0" v-cloak>
					<div class="col-8 d-flex justify-content-center d-flex" > 
						<div id="zoom" style="height: 150px; width: 150px;"></div>		
					</div>
					<div class="col-8 d-flex justify-content-center m--margin-top-20">
						<h4>Il n'y a pas encore d'annonce pour ce match.</h4>
					</div>
					<div class="col-8 d-flex justify-content-center m--margin-top-20 m--margin-bottom-20" >
						<button type="button" class="btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom">Postez-une annonce</button>
						<h6 class="d-flex m--margin-left-20 m--margin-right-20" style="align-items: center; margin-bottom: 0 !important;"> / </h6>
						<button type="button" class="btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom">Soyez avertis des futurs annonces</button>
					</div>
				</div>
				
				<div class="col-8" v-bind:class="{ 'show' : !isLoading }" style="display: none;">
					<div class="row" v-cloak>
						<div class="col-4" v-for="ad in dataAds">
							<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded-force">
								<div class="m-portlet__head m-portlet__head--fit">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-action"></div>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="m-widget19">
										<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" style="min-height-: 286px">							 
											<div class="row d-flex justify-content-center">
												<div class="col-6" style="margin-bottom: 2rem; margin-top: 2rem;">
													<img v-bind:src="getDriverAvatarPath(ad)" alt="" class="avatar avatar-xxl">
												</div>
											</div>

										</div>
										<div class="m-widget19__content">				
											<div class="m-widget19__header m--align-center" style="width: 100%">
												<div class="m-widget19__info">
													<span class="m-widget19__username">
														@{{ ad.user.firstname }} @{{ ad.user.lastname }} @{{ getDriverAge(ad) }}
													</span>
													<br> 
													<el-rate
													 	v-model="ad.user.average_note"
													  	disabled
													  	text-color="#ff9900"
													  	class="m--margin-bottom-20">
													</el-rate>
													<br>
													<span class="m-widget19__username">
														@{{ ad.date_depart_human }} 
													</span>
												</div>
											</div>
											<div class="m-widget19__body">

												<div class="row m--margin-top-5 m--margin-bottom-5">
													<u class="m--margin-right-5">Match </u> : @{{ ad.match.homeTeam.name }} - @{{ ad.match.outsideTeam.name }} 
												</div>
												<div class="row m--margin-top-5 m--margin-bottom-5">
													<u class="m--margin-right-5">Distance </u> : @{{ ad.distance }} km
												</div>
												<div class="row m--margin-top-5 m--margin-bottom-5">
													<u class="m--margin-right-5">Départ </u> : @{{ ad.city_start }} 
												</div>
												<div class="row m--margin-top-5 m--margin-bottom-5">
													<u class="m--margin-right-5">Arrivée </u> : @{{ ad.match.stade.name }} 
												</div>
												<div class="row m--margin-top-5 m--margin-bottom-5">
													<u class="m--margin-right-5">Places </u> : @{{ ad.remaining_places }} restante(s)
												</div>
												<div class="row m--margin-top-5 m--margin-bottom-5">
													<u class="m--margin-right-5">Prix </u> : @{{ ad.unit_price }} €
												</div>
											</div>
										</div>
										<div class="m-widget19__action d-flex justify-content-center">
											<button type="button" class="btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom"v-on:click="goToAdPage(ad)" >Ca m'intéresse</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-8 justify-content-center m--margin-bottom-100 m--margin-top-100" v-bind:class="{ 'd-flex' : isLoading }" v-show="isLoading">
					<div id="voiture_anim" style="height: 80px; width: 80px;"></div>
				</div>
			</div>
		</section>
	</div>
</display-annonces-by-search>
@endsection