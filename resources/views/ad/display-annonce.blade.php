@extends('layouts.app')

@section('content')
<annonce :ad="{{ json_encode($ad) }}" inline-template v-cloak>
	<section class="hero">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col">
					<h2 style="text-align: center; margin-bottom: 30px;">@{{ ad.match.homeTeam.name }} - @{{ ad.match.outsideTeam.name }}</h2>
				</div>
			</div>
			<div class="row d-flex" style="justify-content : space-around;">
				<div class="col-10">
					<div class="row d-flex" style="justify-content : space-around;" id="infoAnnonce">
						<div class="col-4">
							<div class="row d-flex align-items-center">
								<i class="flaticon-pin m--margin-right-15" style="font-size: 2rem"></i>
								@{{ ad.city_start }}
							</div>
							<div class="row d-flex align-items-center">
								<i class="flaticon-placeholder m--margin-right-15" style="font-size: 2rem"></i>
								@{{ ad.match.stade.name }}
							</div>
							<div class="row d-flex align-items-center">
								<i class="flaticon-time-2 m--margin-right-15" style="font-size: 2rem"></i>
								Départ : @{{ ad.date_depart_human }}
							</div>
							<div class="row d-flex align-items-center">
								<i class="flaticon-time m--margin-right-15" style="font-size: 2rem"></i>
								Retour : @{{ ad.date_return }}
							</div>
							<div class="row d-flex align-items-center">
								<i class="flaticon-user-add m--margin-right-15" style="font-size: 2rem"></i>
								Places disponibles : @{{ ad.remaining_places }}
							</div>
						</div>
						<div class="col-4">
							MAP
						</div>
					</div>
				</div>

			</div>

			<div class="row d-flex justify-content-center m--margin-top-50">
				<div class="col-10">
					<div class="row m-row--no-padding m-row--col-separator-xl">
						<div class="col-xl-12">
							<div class="m-widget14">
								<div class="row  align-items-center">
									<div class="col-6 d-flex align-items-center justify-content-center">
										<img v-bind:src="getDriverAvatarPath(ad)"class="avatar avatar-xxl" alt="">
									</div>
									<div class="col-6 d-flex align-items-center">
										<div class="m-widget14__legends">
											<div class="m-widget14__legend">
												<p>@{{ ad.user.firstname }} @{{ ad.user.lastname }}</p>	
											</div>
											<div class="m-widget14__legend">
												<p>@{{ getDriverAge(ad) }}</p>
											</div>
											<div class="m-widget14__legend">
												<el-rate
												 	v-model="ad.user.average_note"
													disabled
													text-color="#ff9900"
													class="">
												</el-rate>
											</div>
											<div class="m-widget14__legend">
												<button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air col-12">Contacter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row flex justify-content-center align-items-center m--margin-top-50">
				Je souhaite réserver 
				<el-input-number class="m--margin-right-10 m--margin-left-10" size="mini" v-model="ad.remaining_places" controls-position="right" :min="1" :max="ad.remaining_places"></el-input-number>
				places pour ce trajet. 
			</div>

			<div class="row d-flex justify-content-center m--margin-top-20">
				<div class="col-3">
					<button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air col-12">Réserver</button>
				</div>
			</div>
		</div>
	</section>
</annonce>
@endsection