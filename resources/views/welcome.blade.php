@extends('layouts.app')

@section('content')
<form-recherche-annonce inline-template>
	<section class="section hero">
		<div class="container">
			<div class="row d-flex">
				<div class="col-6">
					<h2 class="title">
						Et si j'allais voir un match ce week-end
						<span class="text-primary" id="call-to-action-match"></span>
						?
					</h2>
					<p>
						Chaque week-end des centaines de matchs se déroulent en France ! Partager un moment de convivialité avec des supporters qui vous ressemblent et partagez des moments inoubliables.
					</p>
					<form action="/recherche-annonce" method="POST" style="padding : 15px;">
						@csrf
						<div class="row">
							<input id="club-choisi" type="text" name="club-choisi" value="" placeholder="Votre club" required="required" autofocus="autofocus" class="form-control col m--margin-right-5">
							<input type="hidden" name="id-club-choisi" v-model="equipeChoisi.id" id="id-club-choisi">
							<input type="hidden" name="lat_ville" v-model="villeDepart.lat" id="lat_ville">
							<input type="hidden" name="lng_ville" v-model="villeDepart.lng" id="lng_ville">
							<input id="villeDepartRecherche" type="text" ref="villeDepartRecherche" name="villeDepartRecherche" value="" placeholder="Votre ville de départ" required="required" autofocus="autofocus" class="form-control col m--margin-left-5">
						</div>
						<div class="row justify-content-center m--margin-top-15">			
							<button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air col-6">Rechercher</button>
						</div>
					</form>
				</div>
				<div class="col-6">
					<img src="../images/hand_drawn.png" alt="" style="height: 100%; width: 100%;">
				</div>
			</div>
		</div>
	</section>
</form-recherche-annonce>

<div class="intersection">
	<h1 class="m--align-center">Les matchs du weekend</h1> 
</div>

<weekend-match inline-template>
	<section class="section hero">
		<div class="container">
			<div class="row d-flex" style="justify-content: center;" v-cloak>
				<div class="col-10">
					<div class="row">
						<div class="col-4" v-for="match in matchs">
							<div class="m-portlet m-portlet--mobile">
								<div class="m-portlet__head m-portlet__head--fit" style="border-bottom: none !important;">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-action">
											<button type="button" class="btn btn-sm m-btn--pill  btn-brand">A partir de 15 euros</button>
										</div>
									</div>
								</div>
								<div class="m-portlet__body m--align-center">
									<div class="m-widget19">
										<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" style="min-height-: 286px">							 
											<img src="https://picsum.photos/200/200/?random" alt=""> 
											<div class="m-widget19__shadow"></div>
										</div>
										<div class="m-widget19__content">				
											<div class="m-widget19__header">
												<div class="m-widget19__info">
													<span class="m-widget19__username">
														@{{ match.equipeDomicile.nom }} - @{{ match.equipeExterieur.nom }} 
													</span><br> 
													<span class="m-widget19__time">
														11/11/2018 à 21h
													</span>		 
												</div>
												<div class="m-widget19__stats">
													<span class="m-widget19__number m--font-brand">
														18
													</span>
													<span class="m-widget19__comment">
														Annonces
													</span>								 
												</div>
											</div>
											<div class="m-widget19__body"></div>
										</div>
										<div class="m-widget19__action">
											<button type="button" class="btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom">En savoir plus</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
</weekend-match>

<div class="intersection">
	<h1 class="m--align-center">Comment ça fonctionne ?</h1> 
</div>

<section class="section hero m--align-center">
	<div class="row d-flex" style="justify-content: center;">
		<div class="col-10" >
			<div class="row">
				<div class="col">
					<div class="m-portlet how-it-works-portlet">
						<div class="m-portlet__body  m-portlet__body--no-padding">
							<div class="row m-row--no-padding">
								<div class="col-xl-4">
									<div class="m-widget14">
										
										<div class="m-widget14__header m--margin-bottom-30">
											<img src="/svg/zoom.svg" alt="">
										</div>
										<div class="row d-flex" style="justify-content: center;">
											<h3 class="m-widget14__title">
												Chercher un match           
											</h3>
											<span class="m-widget14__desc">
												Toutes les semaines de nouveaux évènements sportifs apparaitront. 
											</span>
										</div>
									</div>			
								</div>
								<div class="col-xl-4">
									<div class="m-widget14">
										
										<div class="m-widget14__header m--margin-bottom-30">
											<img src="/svg/transfert.svg" alt="">
										</div>
										<div class="row d-flex" style="justify-content: center;">
											<h3 class="m-widget14__title">
												Joignez-vous à d'autre supporters            
											</h3>
											<span class="m-widget14__desc">
												Réduisez les coûts de déplacements avec d'autre supporter proche de vous
											</span>
										</div>
									</div>			
								</div>
								<div class="col-xl-4">
									<div class="m-widget14">
										
										<div class="m-widget14__header m--margin-bottom-30">
											<img src="/svg/soccer.svg" alt="">
										</div>
										<div class="row d-flex" style="justify-content: center;">
											<h3 class="m-widget14__title">
												Profitez du match            
											</h3>
											<span class="m-widget14__desc">
												Passer un moment de passion et communion avec des supporters du même club, tout en réduisant les coûts
											</span>
										</div>
									</div>			
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="intersection">
	<h1 class="m--align-center ">Pourquoi choisir StadiumGo ?</h1> 
</div>

<section class="section hero">
	<div class="row d-flex" style="justify-content: center;">
		<div class="col-8">
			<div class="row">
				<div class="col-md-6 d-flex" style="justify-content: center; align-items: center;">
					<figure class="text-center" style="margin: 0 !important;">
						<img src="/svg/earth-care.svg" alt="" style="width: 200px">
					</figure>
				</div>
				<div class="col-md-6 d-flex" style="align-items: center;">
					<article class="">
						<h2 style="text-align: left;">Tous ensemble pour l'écologie. <span>Pour la planète.</span> </h2>
						<p style="text-align: left;">We spent much of our time wrestling with traditional, clunky software products and staffing services that don’t reflect how we wanted to work. This experience inspired us to build a company that  is fundamentally changing the next decades of work.</p>
					</article>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 d-flex" style="align-items: center;">
					<article class="">
						<h2 style="text-align: right;">Démarche économique. <br><span>Pour votre portefeuille</span> </h2>
						<p style="text-align: right;">We spent much of our time wrestling with traditional, clunky software products and staffing services that don’t reflect how we wanted to work. This experience inspired us to build a company that  is fundamentally changing the next decades of work.</p>
					</article>
				</div>
				<div class="col-md-6 d-flex" style="justify-content: center; align-items: center;">
					<figure class="text-center" style="margin: 0 !important;">
						<img src="/svg/economy.svg" alt="" style="width: 200px">
					</figure>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 d-flex" style="justify-content: center; align-items: center;">
					<figure class="text-center" style="margin: 0 !important;">
						<img src="/svg/conversation.svg" alt="" style="width: 200px">
					</figure>
				</div>
				<div class="col-md-6 d-flex" style="align-items: center;">
					<article class="">
						<h2 style="text-align: left;">Esprit d'équipe.</h2>
						<p style="text-align: left;">We spent much of our time wrestling with traditional, clunky software products and staffing services that don’t reflect how we wanted to work. This experience inspired us to build a company that  is fundamentally changing the next decades of work.</p>
					</article>
				</div>
			</div>

		</div>
	</div>
</section>

<div class="intersection">
	<h1 class="m--align-center">Ce qu'ils disent de nous</h1>
</div>

<testimonials inline-template>
	<section class="hero">
		<div class="testimonials">
			<div class="row d-flex" style="justify-content: center;">
				<div class="col-8">
					<div class="row d-flex" style="justify-content: center;" >
						<blockquote class="blockquote">
							<p class="lead-blockquote">Technology is just a tool. In terms of getting the kids working together and motivating them, the teacher is the most important.</p>
							<br>
							<div class="d-flex" style="justify-content: center;">
								<img class="avatar avatar-xl" src="../images/users/default.png" alt="...">
							</div>
							<footer>Marck Zuckerberg</footer>
						</blockquote>
					</div>
				</div>
			</div>
			<div class="row d-flex" style="justify-content: center;">
				<div class="col-8">
					<div class="row d-flex" style="justify-content: center;" >
						<blockquote class="blockquote">
							<p class="lead-blockquote">Pouet.</p>
							<br>
							<div class="d-flex" style="justify-content: center;">
								<img class="avatar avatar-xl" src="../images/users/default.png" alt="...">
							</div>
							<footer>Bill Gates</footer>
						</blockquote>
					</div>
				</div>
			</div>
			<div class="row d-flex" style="justify-content: center;">
				<div class="col-8">
					<div class="row d-flex" style="justify-content: center;" >
						<blockquote class="blockquote">
							<p class="lead-blockquote">StadiumGo c'est juste de la boulette.</p>
							<br>
							<div class="d-flex" style="justify-content: center;">
								<img class="avatar avatar-xl" src="../images/users/default.png" alt="...">
							</div>
							<footer>John Doe</footer>
						</blockquote>
					</div>
				</div>
			</div>
		</div>
	</section>
</testimonials>


<section class="section hero grey">
	<div class="row d-flex" style="justify-content: center;">
		<div class="col-6">
			<div class="row">
				<div class="col" id="call-to-action">
					<h2 class="is-leading m--align-center m--margin-bottom-35">Rejoignez-nous !</h2>
					<h6 class="subtitle m--align-center">StadiumGO vous <strong>accompagne</strong> tout au long de l'année pour suivre votre club préféré.</h6>
					<hr class="w-5 my-7">
					<div class="row justify-content-center m--margin-top-15">
						<a href="/login" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air col-3">
							S'inscrire
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection