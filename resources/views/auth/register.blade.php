@extends('layouts.app')

@section('content')
<register-component inline-template>
    <div class="container">
        <div class="row d-flex justify-content-center">

            <div class="col-lg-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    S'enregistrer
                                </h3>
                            </div>          
                        </div>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">
                            <form method="POST" class="m-form m-form--fit m-form--label-align-right" action="{{ route('register') }}">
                                @csrf
                                
                                <div class="m-portlet__body">
                                    <div class="form-group row m-form__group">
                                        <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('Nom') }}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required autofocus>

                                            @if ($errors->has('firstname'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('firstname') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row m-form__group">
                                        <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Prénom') }}</label>

                                        <div class="col-md-6">
                                            <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autofocus>

                                            @if ($errors->has('lastname'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('lastname') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row m-form__group">
                                        <label for="id_team_supported" class="col-md-4 col-form-label text-md-right">{{ __('Club favori') }}</label>

                                        <div class="col-md-6">
                                            <input type="hidden" id="id_team_supported" name="id_team_supported" v-model="id_team_supported">
                                            <input id="club-favori" type="text" class="form-control{{ $errors->has('id_team_supported') ? ' is-invalid' : '' }}" name="club-favori" value="{{ old('club-favori') }}" required autofocus>

                                            @if ($errors->has('id_team_supported'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('id_team_supported') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row m-form__group">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Adresse mail') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row m-form__group">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                            @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row m-form__group">
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmation de mot de passe') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0 m-form__group m--align-center">
                                        <div class="col-4"></div>
                                        <div class="col-6">
                                            <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">S'inscrire</button>&nbsp;&nbsp;
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</register-component>
@endsection
