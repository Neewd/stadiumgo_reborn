@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex justify-content-center">

        <div class="col-lg-8">
            <div class="m-portlet m-portlet--full-height m-portlet--tabs">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Se connecter
                            </h3>
                        </div>          
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                        <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="m-portlet__body">

                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Adresse mail</label>
                                    <div class="col-8">
                                        <input class="form-control m-input" type="email" name="email" id="email" required autofocus>
                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback m-form__help" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Mot de passe</label>
                                    <div class="col-8">
                                        <input class="form-control m-input" id="password" type="password" name="password">
                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback m-form__help" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label for="" class="col-2 col-form-label"></label>
                                    <div class="col-4">
                                        <label class="m-checkbox m-checkbox--focus">
                                            <input type="checkbox" id="remember" name="remember"> Se souvenir de moi
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="col-4 m--align-right">
                                        <a href="{{ route('password.request') }}" id="m_login_forget_password" class="m-link">Mot de passe oublié ?</a>
                                    </div>
                                </div>

                                <div class="row form-group m-form__group m--align-center" style="padding-top : 0 !important;">
                                    <div class="col-2">
                                    </div>
                                    <div class="col-7">
                                        <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Se connecter</button>&nbsp;&nbsp;
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
