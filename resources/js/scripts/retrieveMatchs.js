const puppeteer = require('puppeteer')
const cheerio = require('cheerio')
const axios = require('axios')
let ProgressBar = require('ascii-progress');

let baseUrl = "http://127.0.0.1:8000"

let data = {
  grant_type: 'client_credentials',
  client_id: 1,
  client_secret: 'JEV1wCIbXLpkK2wk0cF8wMLlFKTFEPkpsbqrspHD',
  scope: '',
}

const GOOGLE_API_KEY = "AIzaSyCg75_dBQUih8G5iT6SxkxvEWTiHiMsxlU"

// Récupération des matchs 
async function getMatchs(config) {

  let sport = config.sport;
  let pays = config.pays;
  let competition = config.competition;
  let fonction = 'calendrier';
  const url = 'https://www.flashresultats.fr/' + sport + '/' + pays + '/' + competition + '/' + fonction + '/';

  const browser = await puppeteer.launch();
  const mainPage = await browser.newPage();
  await mainPage.setRequestInterception(true);

  mainPage.on('request', (request) => {
    if (['image', 'stylesheet', 'font'].indexOf(request.resourceType()) !== -1) {
      request.abort()
    } else {    
      request.continue()
    }

  })

  await mainPage.goto(url,{ timeout: 0});

  let $ = cheerio.load(await mainPage.content())

  let tableOfMatchs = {}  
  let tableOfMatchsOfJourney = []
  let currentJourney = -1

  let trs = $('#fs-fixtures > table > tbody > tr')

  let numberOfJourneyIWant = 3
  let numberOfMatchIWant =(trs.length / 11) > numberOfJourneyIWant ? (numberOfJourneyIWant * 11) : trs.length
  
  let bar = new ProgressBar({ 
      schema: 'Récupération.white des.white matchs.white de.white ' + competition + ' : [.white:bar] :current/:total :percent :elapseds :etas',
      total : numberOfMatchIWant - numberOfJourneyIWant
  });

  for (let i = 0; i < numberOfMatchIWant; i++) {

    // Récupération des numéros de journées
    if (trs[i].attribs.class == 'event_round') {
    	if (trs[i].children[0].children[0].data.trim().split(' ')[1] != currentJourney) {
    		if (currentJourney != -1) {
    			tableOfMatchs[currentJourney] = tableOfMatchsOfJourney
    		}
    		currentJourney = trs[i].children[0].children[0].data.trim().split(' ')[1]

    		tableOfMatchsOfJourney = []
    	}
    }

    // Récupération des matchs de la journée
    if (trs[i].attribs.class.match('stage-scheduled')) {
      let dataMatch = {}

      dataMatch.idDuMatch = trs[i].attribs.id.split('_')[2]
      let urlResumeMatch = 'https://www.flashresultats.fr/match/' + dataMatch.idDuMatch + '/#resume-du-match';
      dataMatch.equipeDomicile = trs[i].children[2].children[0].children[0].data
      dataMatch.equipeExterieur = trs[i].children[3].children[0].children[0].data

      // On créé une page pour récupérer les informations sur le stade du match et l'heure
      const pageOfTheMatch = await browser.newPage();
      await pageOfTheMatch.setRequestInterception(true);

      pageOfTheMatch.on('request', (request) => {
        if (['image', 'stylesheet', 'font'].indexOf(request.resourceType()) !== -1) {
          request.abort()
        } else {    
          request.continue()
        }
      })

      await pageOfTheMatch.goto(urlResumeMatch);
      await pageOfTheMatch.waitFor(2000)
      let $newPage = cheerio.load(await pageOfTheMatch.content())

      let dateHeure = $newPage('#utime')[0].children[0].data
      dataMatch.date = dateHeure.split(' ')[0]
      dataMatch.heure = dateHeure.split(' ')[1]

      let stadiumData = await retrieveStadiumAddress($newPage)

      //dataMatch.stadeName = stadiumData.name
      // dataMatch.adresseDuStade = stadiumData.address
      dataMatch.stade = {
        'name' : stadiumData.name,
        'country' : stadiumData.country,
        'streetNumber' : stadiumData.streetNumber,
        'route' : stadiumData.route,
        'city' : stadiumData.city,
        'zip_code' : stadiumData.zip_code,
        'adresseDuStade' : stadiumData.address
      }

      await pageOfTheMatch.close();

      tableOfMatchsOfJourney.push(dataMatch)
    }

    bar.tick();

    // Si nous sommes après le dernier match, nous pouvons rajouter le tableau de matchs de la journée au tableaux total
    if (i == numberOfMatchIWant - 1) {
      tableOfMatchs[currentJourney] = tableOfMatchsOfJourney
    }

  }

  let metadata =  {
    'competition' : config.competition,
    'pays' : config.pays,
    'sport' : config.sport
  }

  let data = {
    'metadata' : metadata,
    'matchs' : tableOfMatchs
  }

  await browser.close();

  return data;
}

// Récupération de la config token
async function getToken() {

  let headers = {}
  try {
    let bearerToken = await axios.post(baseUrl + '/oauth/token', data)
    headers = {
      'authorization' : 'Bearer ' + bearerToken.data.access_token ,
      'accept' : 'application/json'
    }
  } catch (exception) {
    throw (exception.response.data.error + " => " + exception.response.data.message)
  }

  return headers;
}

// Récupération des compétitions à ramener etc
async function getScriptConfig(headers) {

  let result
  try {
    result = await axios.get(baseUrl + '/api/client/script-config',{ headers : headers })
  } catch (exception) {
    throw "Aucune configuration n'a été récupéré : " + exception.response.data.exception
  }

  return result.data;
}

// Main function
async function doAll() {
  let headers = await getToken()
  let config = await getScriptConfig(headers)

  console.log("Let's begin !")
  for (let indexConfig = 0; indexConfig < config.length; indexConfig++) {
    let lesMatchs = await getMatchs(config[indexConfig])
    console.log("Envoi des matchs au serveur")
    axios.post( baseUrl + '/api/client/matchs', lesMatchs, { headers : headers }).then(result => {
      //console.log(result.data)
    }).catch(error => {
      console.log(error)
    })
  }
}

doAll()

// Fonction utils permettant de remplacer toutes les caractères que l'on souhaite par quelque chose d'autre.
// Très utile pour la création d'URL.
String.prototype.replaceAll = function(target, replacement) {
  return this.split(target).join(replacement);
};

// Récupère les informations sur le stade 
async function retrieveStadiumAddress($newPage) {
  let subContentOfNewPage = $newPage('#summary-content > div.match-information-wrapper > table > tbody > tr.content')
  let stadiumData = {}

  // Dans le cas où il n'y a aucune information sur le stade
  if (subContentOfNewPage.length == 0) {
    stadiumData.name = ''
    stadiumData.formattedAddress = ''
    stadiumData.zip_code = ''
    stadiumData.city = ''
    stadiumData.country = ''
    stadiumData.route = ''
    stadiumData.streetNumber = ''
    return stadiumData
  }

  for (let i = 0; i < subContentOfNewPage.length; i++) {
    // Dans le cas où tout est rempli
    for (let j = 0; j < subContentOfNewPage[i].children.length; j++) {

      if (subContentOfNewPage[i].children[j].type == 'tag') {
        if (subContentOfNewPage[i].children[j].children[0].data.trim().match('Stade:\\s(.*)')) {
          stadiumData.name = subContentOfNewPage[i].children[j].children[0].data.trim().match('Stade:\\s(.*)')[1]
          let urlGoogleApi = "https://maps.googleapis.com/maps/api/geocode/json?address=" + stadiumData.name.replaceAll(' ', '+') + "&key=" + GOOGLE_API_KEY
          let responseFromThisAwesomeApi = await axios.get(encodeURI(urlGoogleApi))

          if (responseFromThisAwesomeApi.data.error_message == null) {
            // On parcours le tableau address_components
            for (let j = 0; j < responseFromThisAwesomeApi.data.results[0].address_components.length; j++) {
              switch(responseFromThisAwesomeApi.data.results[0].address_components[j].types[0]) {
                case 'street_number' : 
                stadiumData.streetNumber = responseFromThisAwesomeApi.data.results[0].address_components[j].long_name;
                break;
                case 'route' : 
                stadiumData.route = responseFromThisAwesomeApi.data.results[0].address_components[j].long_name;
                break;
                case 'locality' : 
                stadiumData.city = responseFromThisAwesomeApi.data.results[0].address_components[j].long_name;
                break;
                case 'country' : 
                stadiumData.country = responseFromThisAwesomeApi.data.results[0].address_components[j].long_name;
                break;
                case 'postal_code' : 
                stadiumData.zip_code = responseFromThisAwesomeApi.data.results[0].address_components[j].long_name;
                break;
              }    
            }

            stadiumData.address = responseFromThisAwesomeApi.data.results[0].formatted_address
          } else {
            stadiumData.formattedAddress = ''
          }

          return stadiumData
        }
      }
    }
  }
}