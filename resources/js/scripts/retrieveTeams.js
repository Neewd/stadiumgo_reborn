const puppeteer = require('puppeteer')
const cheerio = require('cheerio')
const axios = require('axios')
let ProgressBar = require('ascii-progress')

let baseUrl = "http://127.0.0.1:8000"

let data = {
  grant_type: 'client_credentials',
  client_id: 2,
  client_secret: 'm5eLgeb2Rcu286QySKkAQYEPoGSm6XMKBKsjqnhq',
  scope: '',
};

// Récupération de la config token
async function getToken() {

  let headers = {} 
  try {
    let bearerToken = await axios.post(baseUrl + '/oauth/token', data)
    headers = {
      'authorization' : 'Bearer ' + bearerToken.data.access_token ,
      'accept' : 'application/json'
    }
  } catch(ex) {

    throw (ex.response.data.error + " => " + ex.response.data.message)

  }
  return headers;
}

// Récupération des compétitions à ramener etc
async function getScriptConfig(headers) {
  let result
  try {
    result = await axios.get(baseUrl + '/api/client/script-config',{ headers : headers })
  } catch (exception) {
    throw "Aucune configuration n'a été récupéré : " + exception.response.data.exception
  }

  return result.data;
}

// Récupération des matchs 
async function getTeams(config) {

  	let sport = config.sport;
  	let pays = config.pays;
  	let competition = config.competition;
  	let fonction = 'classement';

  	const url = 'https://www.flashresultats.fr/' + sport + '/' + pays + '/' + competition + '/' + fonction + '/';

  	const browser = await puppeteer.launch();
  	const mainPage = await browser.newPage();
    await mainPage.setRequestInterception(true);

    mainPage.on('request', (request) => {
      if (['image', 'stylesheet', 'font'].indexOf(request.resourceType()) !== -1) {
        request.abort()
      } else {    
        request.continue()
      }

    })

  	await mainPage.goto(url, { timeout : 0});

  	let $ = cheerio.load(await mainPage.content())

  	let trs = $('#table-type-1 > tbody > tr')

  	let bar = new ProgressBar({ 
  	    schema: 'Récupération.white des.white équipes.white de.white ' + competition + ' : [.white:bar] :current/:total :percent :elapseds :etas',
  	    total : trs.length
  	});
  	
  	let teams = []

  	for (let i = 0; i < trs.length; i++) {

	    let dataTeam = {}

	    dataTeam.id = trs[i].attribs.class.split('-')[2];
	    dataTeam.name = trs[i].children[1].children[1].children[0].children[0].data;
      dataTeam.pays = pays;
	    let urlDetailTeam = 'https://www.flashresultats.fr' + trs[i].children[1].children[1].children[0].attribs.onclick.match("javascript\\:getUrlByWinType\\('(.*)'\\);")[1];

	    // On créé une page pour récupérer les informations sur le stade du match et l'heure
	    const pageOfTheTeam = await browser.newPage();

	    await pageOfTheTeam.goto(urlDetailTeam, { timeout : 0 })

	    await pageOfTheTeam.waitFor(300);
	    let $newPage = cheerio.load(await pageOfTheTeam.content());

	    let style = $newPage('#fscon > div.team-header > div.team-logo').attr('style')
	    let urlLogo = "https://www.flashresultats.fr" + style.match('background\\-image\\:\\surl\\((.*)\\)')[1]
	    dataTeam.urlLogo = urlLogo

	    await pageOfTheTeam.close();

	    teams.push(dataTeam)

    	bar.tick();

  	}

  	let metadata =  {
  		'competition' : config.competition,
  		'pays' : config.pays,
  		'sport' : config.sport
  	}

  	let data = {
  		'metadata' : metadata,
  		'teams' : teams
  	}

  	await browser.close();

  	return data;
}

// Main function
async function doAll() {
  let headers = await getToken()
  let config = await getScriptConfig(headers)  
  console.log("Let's begin !")

  for (let indexConfig = 0; indexConfig < config.length; indexConfig++) {
    let toutesLesEquipes = await getTeams(config[indexConfig])
    console.log('Envoi au serveur des équipes de : ' + toutesLesEquipes.metadata.competition + ' !')
      axios.post( baseUrl + '/api/client/teams', toutesLesEquipes, { headers : headers }).then(result => {
        // console.log(result)
        return;
      }).catch(error => {
        console.log(error)
      })
    
  }
}

doAll()