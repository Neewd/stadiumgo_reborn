import Vuex from 'vuex'
import Vue from 'vue'
import axios from 'axios'
import _ from 'lodash'

Vue.use(Vuex)

// Store Modules
import teamModule from './modules/team'
	
export default new Vuex.Store({
	modules : {
		teams : teamModule
	}
})