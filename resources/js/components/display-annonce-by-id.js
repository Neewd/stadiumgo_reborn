Vue.component('annonce', {
	props: ['ad'],
	data() {
		return {

		}
	},
	methods : {
		getDriverAvatarPath(ad) {
			return '/images/users/' + ad.user.avatar_path
		},
		getDriverAge(ad) {
			let birthdate = new Date(ad.user.birthdate)
			let today = Date.now()
			var ageDifMs = today - birthdate.getTime()
		    var ageDate = new Date(ageDifMs)
		    return Math.abs(ageDate.getUTCFullYear() - 1970) + ' ans'
		}
	}
})