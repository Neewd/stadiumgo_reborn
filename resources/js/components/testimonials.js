Vue.component('testimonials', {
	data() {
		return {
			testimonials : []
		}
	},
	beforeMount() {
		// Ici il faut aller chercher les testimonials
	},
	mounted() {
		$('.testimonials').slick({
	    	dots: false,		  	
	    	infinite: true,
			speed: 1000,
			slidesToShow: 1,
			adaptiveHeight: true
		});
	}
});