import moment from 'moment'

Vue.component('proposer-trajet', {

	data() {
		return {
            // Général
			teams : [],
			steps : [ 'Choisir son club', 'Ville de départ', 'Date & Horaire', 'Prix & Place', 'Récapitulatif' ],
			activeStepIndex : 0,
            googleGeocoder : null,
            googleDirectionsService : null,
            googleDirectionsRenderer : null,
            markerStade : null,
            markerVilleDepart : null,
            map : null,
            center: {
                lat: 46.7167,
                lng: 2.5167
            },

			// First step : 
			clubChoisiNom : '',
			matchs : [],
            matchChoisi : {
                id: '',
                equipeDomicile : {
                    nom : '',
                    stade : ''
                },
                equipeExterieur : {
                    nom : ''
                },
                date : null,
                heure : ''
            },

            // Second & Third step 
            departInfo : {
                ville : '',
                heure : '',
                lat : 0,
                lng : 0
            },
            distance : '',
            tempsTrajet : '',
            isAllerRetour : true,
            returnInfo : {
                directAfterMatch : false,
                date : null,
                heure : ''
            },

            // Fourth step
            nbPlaces : 1,
            prixUnite : 1,
		} 
	},
	beforeCreate() {
		// On va récupérer les équipes
		let url = '/api/v1/team'
		window.axios.get(url)
			.then(response => {
				this.teams = response.data
			})
			.catch(error => {
				console.log(error)
			})
    },
    mounted() {

        $('#departClockpicker').clockpicker({
            autoclose: true
        }).on('change', () => { this.departInfo.heure = $('#departClockpicker').val() })

        const options = {
            zoom: 5,
            center: new google.maps.LatLng(this.center.lat,this.center.lng),
            //styles: styleOfTheMaps
        }

        const optionsAutocomplete = {
          componentRestrictions: {country: ["fr", 'mc']}
         }

        this.googleDirectionsService = new google.maps.DirectionsService
        this.googleDirectionsRenderer = new google.maps.DirectionsRenderer
        this.map = new google.maps.Map(document.getElementById('mapFrance'), options)
        this.googleDirectionsRenderer.setMap(this.map)
        let autocomplete = new google.maps.places.Autocomplete(this.$refs.villeDeDepartInput, optionsAutocomplete)

        let self = this
        autocomplete.addListener('place_changed', function() {
            let place = autocomplete.getPlace()
            self.googleGeocoder.geocode( { 'address' : place.formatted_address }, function( results, status ) {
                if( status == google.maps.GeocoderStatus.OK ) { 
                    self.departInfo.ville = results[0].formatted_address
                    self.departInfo.lat = results[0].geometry.location.lat()
                    self.departInfo.lng = results[0].geometry.location.lng()
                    self.placeMarker(results[0], 'markerVilleDeDepart')
                    self.calculateAndDisplayRoute()
                    self.computeDistanceBetweenOriginAndDestination().then(data => {
                        self.tempsTrajet = data.duree
                        self.distance = data.distance                     
                    }).catch(error => {
                        console.log(error)
                    })
                } else {
                    console.log("Erreur autocomplete : " + status)
                }   
            })
        })
    },
    methods: {
    	loadClubMatchs(teams) {
    		let substringMatcher = function(strs) {
    			return function findMatches(q, cb) {
    				let matches = []
    				let substringRegex
    				let substrRegex = new RegExp(q, 'i')
    				$.each(strs, function(i, str) {
    					if (substrRegex.test(str.name)) {
    						matches.push(str)
    					} 
    				})
    				cb(matches)
    			}
    		}

    		$('#club-choisi').typeahead({
    			hint: true,
    			highlight: true,
    			minLength: 1
    		},
    		{
    			name: 'teams',
    			minLength: 1,
    			limit: 10,
    			source: substringMatcher(teams),
    			display: function(item){
    				return item.name
    			},
    			templates: {
    				empty: [
    				'<div class="empty-message">',
    				"Nous n'arrivons pas à trouver cette équipe. ",
    				'</div>'
    				].join('\n'),
    				suggestion: function (data) {
    					return '<div class="gap-x" style="display : flex; justify-content: flex-start; align-items: center;"><img style="width:25px !important; height: 25px !important; " src="/images/teams/' + data.logo + '" alt="logo" /><div style="max-width:50%; margin-left : 8px;">' + data.name  +'</div></div>';
    				}
    			}
    		})

    		let self = this
    		$('#club-choisi').bind('typeahead:select', function(ev, suggestion) {
    			self.clubChoisiNom = suggestion.name
    			window.axios.get('api/v1/match/team/' + suggestion.id)
    				.then(response => {
    					self.matchs = response.data
    				})
    				.catch(error => {
    					console.error(error)
    				})
    		})
    	},
        getMatchId(id) {
            return 'match-' + id
        },
        selectMatch(match) {
            this.matchChoisi.id = match.id
            this.matchChoisi.equipeDomicile.nom = match.homeTeam.name
            this.matchChoisi.equipeDomicile.stade = match.stade.name
            this.matchChoisi.equipeExterieur.nom = match.outsideTeam.name
            this.matchChoisi.date = match.date
            this.matchChoisi.heure = match.heure
                
            this.googleGeocoder = new google.maps.Geocoder()
            let self = this
            this.googleGeocoder.geocode( { 'address' : this.matchChoisi.equipeDomicile.stade }, function( results, status ) {
                if( status == google.maps.GeocoderStatus.OK ) {
                    self.placeMarker(results[0], 'markerStade')                 
                } else {
                    console.log( 'Geocode was not successful for the following reason: ' + status );
                }
            })
        },
        goToStep(step) {
            this.activeStepIndex = step

            if (step == 2) {

                $('#departDatePicker').datepicker({
                    format: 'dd/mm/yyyy',
                    endDate : this.matchChoisi.date 
                }).on(
                    'changeDate', () => { this.departInfo.jour = $('#departDatePicker').val() }
                )

                $('#returnDatePicker').datepicker({
                    startDate : this.matchChoisi.date 
                }).on(
                    'changeDate', () => { this.returnInfo.jour = $('#returnDatePicker').val() }
                )

                $('#retourClockPicker').clockpicker({
                    autoclose: true
                }).on('change', () => { this.returnInfo.heure = $('#retourClockPicker').val() })

            }

            if (step == 3) {
                if (this.returnInfo.directAfterMatch) {
                    let momentDePartir = moment(this.matchChoisi.date + ' ' + this.matchChoisi.heure, 'DD/MM/YYYY HH:ss').add(3, 'hours').format('DD/MM/YYYY HH:ss')
                    this.returnInfo.jour = momentDePartir.split(' ')[0]
                    this.returnInfo.heure = momentDePartir.split(' ')[1]
                }
            } 
        },
        calculateAndDisplayRoute() {
            let self = this
            this.googleDirectionsService.route({
                origin: self.matchChoisi.equipeDomicile.stade,
                destination: self.departInfo.ville,
                travelMode: 'DRIVING'
            }, function(response, status) {
                if (status === 'OK') {
                    self.googleDirectionsRenderer.setDirections(response)
                } else {
                    window.alert('Directions request failed due to ' + status)
                }
            })
        },
        placeMarker(place, markerName) {
            let marker = new google.maps.Marker({
                map: this.map,
                anchorPoint: new google.maps.Point(place.geometry.location.lng(), place.geometry.location.lat())
            })
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                this.map.fitBounds(place.geometry.viewport)
            } else {
                this.map.setCenter(place.geometry.location)
                this.map.setZoom(8)  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location)
            marker.setVisible(true)
            if (markerName == 'markerStade') {
                this.markerStade = marker
            } 
            if (markerName == 'markerVilleDeDepart') {
                this.markerVilleDepart = marker
            }
        },
        computeDistanceBetweenOriginAndDestination() {
            return new Promise((resolve, reject) => {
                let matrixDistanceService = new google.maps.DistanceMatrixService
                let origin = {lat: this.markerVilleDepart.getPosition().lat(), lng: this.markerVilleDepart.getPosition().lng()}
                let destination = {lat: this.markerStade.getPosition().lat() , lng: this.markerStade.getPosition().lng() }

                let self = this
                matrixDistanceService.getDistanceMatrix({
                    origins: [origin],
                    destinations: [destination],
                    travelMode: 'DRIVING',
                    unitSystem: google.maps.UnitSystem.METRIC,
                    avoidHighways: false,
                    avoidTolls: false
                }, function(response, status) {
                    if (status !== 'OK') {
                        reject('Error was: ' + status);
                    } else {
                        let originList = response.originAddresses
                        let destinationList = response.destinationAddresses
                        resolve({ 'distance' : response.rows[0].elements[0].distance.value / 1000 , 'duree' : response.rows[0].elements[0].duration.text,  })
                    }
                })
            })
        },
        decrementActiveStepIndex() {
            if (this.activeStepIndex > 0) {
                this.activeStepIndex--
            }
        },
        registerAd() {
            let url = "/api/v1/ad"
            window.axios.post(url, this.ad )
            .then(response => {
                document.location.href = '/dashboard'  
                window.swal("Votre trajet à bien été crée.", "Vous allez recevoir un mail de confirmation d'ici peu.", "success")
            })
            .catch(error => {
                console.log(error)
            })
        }
    },
    computed: {
        canWeGoNext() {
            switch(this.activeStepIndex) {
                case 0 : 
                    if (this.matchChoisi.id == '') {
                        return false
                    } else {
                        return true
                    }
                    break;
                case 1 : 
                    if (this.departInfo.ville == '') {
                        return false
                    } else {
                        return true
                    }
                    break; 
                case 2 : 
                    if (this.departInfo.jour != "" && this.departInfo.heure != "") {
                        return true
                    } else {
                        return false
                    }
                    break;
                case 3 : 
                    if (this.nbPlaces >= 1 && this.prixUnite > 1) {
                        return true
                    } else {
                        return false                        
                    }
                    break;  
                default : {
                    return false
                }
            }
        },
        matchConcerne() {
            return this.matchChoisi.equipeDomicile.nom + ' - ' + this.matchChoisi.equipeExterieur.nom
        },
        ad() {
            return {
                city_start : this.departInfo.ville,
                lat_city_start : this.departInfo.lat,
                lng_city_start : this.departInfo.lng,
                date_depart : moment(this.departInfo.jour + ' ' + this.departInfo.heure, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss'),
                total_nb_places : this.nbPlaces,
                remaining_places : this.nbPlaces,
                unit_price : this.prixUnite,
                distance : this.distance,
                duration : this.tempsTrajet,
                user_id : this.$refs.userConnected.value,
                match_id : this.matchChoisi.id,
                going_coming : this.isAllerRetour,
                return_after_match : this.returnInfo.directAfterMatch,
                date_return: this.isAllerRetour ? moment(this.returnInfo.jour + ' ' + this.returnInfo.heure, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss') : null,
            }
        }
    },
	watch : {
		teams : {
			handler(teams) {
				this.loadClubMatchs(teams)
			}
		}
	}
});
