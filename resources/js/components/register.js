Vue.component('register-component', {

	data() {
		return {
			teams : [],
			id_team_supported : ''
		} 
	},
	beforeMount() {
		// On va récupérer les équipes
		let url = '/api/v1/team'
		window.axios.get(url)
		.then(response => {
			this.teams = response.data
		})
		.catch(error => {
			console.log(error)
		})
	},
	mounted() {
        //
    },
    methods: {
    	loadClubFavori(teams) {
    		let substringMatcher = function(strs) {
    			return function findMatches(q, cb) {
    				let matches = []
    				let substringRegex
    				let substrRegex = new RegExp(q, 'i')
    				$.each(strs, function(i, str) {
    					if (substrRegex.test(str.name)) {
    						matches.push(str)
    					} 
    				})
    				cb(matches)
    			}
    		}

    		$('#club-favori').typeahead({
    			hint: true,
    			highlight: true,
    			minLength: 1
    		},
    		{
    			name: 'teams',
    			minLength: 1,
    			limit: 10,
    			source: substringMatcher(teams),
    			display: function(item){
    				return item.name
    			},
    			templates: {
    				empty: [
    				'<div class="empty-message">',
    				"Nous n'arrivons pas à trouver cette équipe. ",
    				'</div>'
    				].join('\n'),
    				suggestion: function (data) {
    					return '<div class="gap-x" style="display : flex; justify-content: flex-start; align-items: center;"><img style="width:25px !important; height: 25px !important; " src="/images/teams/' + data.logo + '" alt="logo" /><div style="max-width:50%; margin-left : 8px;">' + data.name  +'</div></div>';
    				}
    			}
    		})

    		let self = this
    		$('#club-favori').bind('typeahead:select', function(ev, suggestion) {
    			self.clubChoisi = suggestion.name
    			self.id_team_supported = suggestion.id
    			//self.getTeamMatchs(suggestion.id).then(response => {
    			//	self.matchs = response
    			//})
    		})
    	}
    },
    watch : {
		teams : {
			handler(teams) {
				this.loadClubFavori(teams)
			}
		}
	}
});
