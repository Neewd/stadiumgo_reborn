import { mapGetters, mapActions } from 'vuex'

Vue.component('form-recherche-annonce', {
	data() {
		return {
			equipeChoisi : {
				id: '',
				name : ''
			},
            villeDepart : {
                lat : 0,
                lng : 0
            },
            googleData : google
		} 
	},
	beforeMount() {

		// On va récupérer les équipes, google et googleGeocoder dans le store
        if (this.teams.length == 0) {
            this.fetchTeams()
        }

    },
    computed: {
        ...mapGetters(['teams'])
    },
	mounted() {

        const optionsAutocomplete = {
          componentRestrictions: {country: ["fr", 'mc']}
         }

        let autocomplete = new this.googleData.maps.places.Autocomplete(this.$refs.villeDepartRecherche, optionsAutocomplete)
        let googleGeocoder = new this.googleData.maps.Geocoder

        let self = this
        autocomplete.addListener('place_changed', function() {
            let place = autocomplete.getPlace()
            googleGeocoder.geocode( { 'address' : place.formatted_address }, function( results, status ) {
                if( status == self.googleData.maps.GeocoderStatus.OK ) {
                    self.villeDepart.lat = results[0].geometry.location.lat()
                    self.villeDepart.lng = results[0].geometry.location.lng()
                } else {
                    console.log("Erreur autocomplete : " + status)
                }   
            })
        })

	},
	methods : {
        ...mapActions(['fetchTeams']),
		loadClubMatchs(teams) {
    		let substringMatcher = function(strs) {
    			return function findMatches(q, cb) {
    				let matches = []
    				let substringRegex
    				let substrRegex = new RegExp(q, 'i')
    				$.each(strs, function(i, str) {
    					if (substrRegex.test(str.name)) {
    						matches.push(str)
    					} 
    				})
    				cb(matches)
    			}
    		}

    		$('#club-choisi').typeahead({
    			hint: true,
    			highlight: true,
    			minLength: 1
    		},
    		{
    			name: 'teams',
    			minLength: 1,
    			limit: 10,
    			source: substringMatcher(teams),
    			display: function(item){
    				return item.name
    			},
    			templates: {
    				empty: [
    				'<div class="empty-message">',
    				"Nous n'arrivons pas à trouver cette équipe. ",
    				'</div>'
    				].join('\n'),
    				suggestion: function (data) {
    					return '<div class="gap-x" style="display : flex; justify-content: flex-start; align-items: center;"><img style="width:25px !important; height: 25px !important; " src="/images/teams/' + data.logo + '" alt="logo" /><div style="max-width:50%; margin-left : 8px;">' + data.name  +'</div></div>';
    				}
    			}
    		})

    		let self = this
    		$('#club-choisi').bind('typeahead:select', function(ev, suggestion) {
    			self.equipeChoisi.name = suggestion.name
    			self.equipeChoisi.id = suggestion.id
    		})
		}
	},
	watch : {
		teams : {
			handler(teams) {
				this.loadClubMatchs(teams)
			}
		}
	}
})