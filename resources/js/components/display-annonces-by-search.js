import lottie from 'lottie-web';

Vue.component('display-annonces-by-search', {
	props: ['ads', 'ville', 'club'],
	data() {
		return {
            googleGeocoder : new google.maps.Geocoder(),
            isLoading : true,
			searchCriteria : {
				club : {
					name : '',
					id : ''
				},
				ville : {
                    name : '',
                    lat : null,
                    lng : null,
                }
			},
            dataAds : [],
			teams : []
		}
	},
	beforeCreate() {
		// On va récupérer les équipes
		let url = '/api/v1/team'
		window.axios.get(url)
			.then(response => {
				this.teams = response.data
			})
			.catch(error => {
				console.log(error)
			})
    },
	mounted() {
		this.searchCriteria.ville.name = this.ville.name
        this.searchCriteria.ville.lat = this.ville.lat
        this.searchCriteria.ville.lng = this.ville.lng

		this.searchCriteria.club.name = this.club.name
        this.searchCriteria.club.id = this.club.id 

        this.dataAds = this.ads

        const optionsAutocomplete = {
          componentRestrictions: {country: ["fr", 'mc']}
         }

        let autocomplete = new google.maps.places.Autocomplete(this.$refs.villeDepartRecherche, optionsAutocomplete)

        let self = this
        autocomplete.addListener('place_changed', function() {
            let place = autocomplete.getPlace()
            self.googleGeocoder.geocode( { 'address' : place.formatted_address }, function( results, status ) {
                if( status == google.maps.GeocoderStatus.OK ) {
                    self.searchCriteria.ville.lat = results[0].geometry.location.lat()
                    self.searchCriteria.ville.lng = results[0].geometry.location.lng()
                    self.searchCriteria.ville.name = results[0].formatted_address
                } else {
                    console.log("Erreur autocomplete : " + status)
                }   
            })
        })

        let animationVoiture = lottie.loadAnimation({
            container: document.getElementById('voiture_anim'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: '/images/voiture_anim.json'
        })

        let animationNotFound = lottie.loadAnimation({
            container: document.getElementById('zoom'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: '/images/zoom.json'
        })

        this.isLoading = false

	},
	methods : {
		getDriverAvatarPath(ad) {
			return '/images/users/' + ad.user.avatar_path
		},
		getDriverAge(ad) {
			return ', 26 ans'
		},
        getMatchsForSearch() {
            this.isLoading = true
            let url = "api/v1/ad/search/" + this.searchCriteria.club.id + "/" + this.searchCriteria.ville.lat + "/" + this.searchCriteria.ville.lng 
            window.axios.get(url)
                .then(response => {
                    this.dataAds = response.data
                    this.isLoading = false
                })
                .catch(error => {
                    console.error(error)
                })

        },
		loadClubMatchs(teams) {
    		let substringMatcher = function(strs) {
    			return function findMatches(q, cb) {
    				let matches = []
    				let substringRegex
    				let substrRegex = new RegExp(q, 'i')
    				$.each(strs, function(i, str) {
    					if (substrRegex.test(str.name)) {
    						matches.push(str)
    					} 
    				})
    				cb(matches)
    			}
    		}

    		$('#club-choisi').typeahead({
    			hint: true,
    			highlight: true,
    			minLength: 1
    		},
    		{
    			name: 'teams',
    			minLength: 1,
    			limit: 10,
    			source: substringMatcher(teams),
    			display: function(item){
    				return item.name
    			},
    			templates: {
    				empty: [
    				'<div class="empty-message">',
    				"Nous n'arrivons pas à trouver cette équipe. ",
    				'</div>'
    				].join('\n'),
    				suggestion: function (data) {
    					return '<div class="gap-x" style="display : flex; justify-content: flex-start; align-items: center;"><img style="width:25px !important; height: 25px !important; " src="/images/teams/' + data.logo + '" alt="logo" /><div style="max-width:50%; margin-left : 8px;">' + data.name  +'</div></div>';
    				}
    			}
    		})

    		let self = this
    		$('#club-choisi').bind('typeahead:select', function(ev, suggestion) {
    			self.searchCriteria.club.name = suggestion.name
    			self.searchCriteria.club.id = suggestion.id
    		})
		},
        goToAdPage(ad) {
            document.location.href = '/annonce/' + ad.id
        }
	},
	watch : {
		teams : {
			handler(teams) {
				this.loadClubMatchs(teams)
			}
		},
        isLoading : {
            handler(value) {

            }
        } 
	}
})