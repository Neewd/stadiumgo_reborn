
window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    // require('bootstrap');
    require('./typeahead.js')

	/**
	* Vendor Metronics
	*/
	// Global Mandatory Vendors -->
	require("../vendors/popper.js/dist/umd/popper.js")
	require("../vendors/bootstrap/dist/js/bootstrap.min.js")
	require("../vendors/js-cookie/src/js.cookie.js")
	require("../vendors/tooltip.js/dist/umd/tooltip.min.js")
	require("../vendors/wnumb/wNumb.js")

	// Global Optional Vendors -->
	require("../vendors/jquery.repeater/src/lib.js")
	require("../vendors/jquery.repeater/src/jquery.input.js")
	require("../vendors/jquery.repeater/src/repeater.js")
	require("../vendors/jquery-form/dist/jquery.form.min.js")
	require("../vendors/block-ui/jquery.blockUI.js")
	require("../vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")
	require("../vendors/clockpicker/dist/bootstrap-clockpicker.min.js")
	require("../vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js")
	require("../vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js")
	require("../vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js")
	require("../vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js")
	require("../vendors/bootstrap-daterangepicker/daterangepicker.js")
	require("../vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js")
	require("../vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js")
	require("../vendors/bootstrap-maxlength/src/bootstrap-maxlength.js")
	// require("../vendors/bootstrap-switch/dist/js/bootstrap-switch.js")
	require("../vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js")
	require("../vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js")
	require("../vendors/bootstrap-select/dist/js/bootstrap-select.js")
	require("../vendors/select2/dist/js/select2.full.js")
	require("../vendors/typeahead.js/dist/typeahead.bundle.js")
	require("../vendors/handlebars/dist/handlebars.js")
	require("../vendors/inputmask/dist/jquery.inputmask.bundle.js")
	require("../vendors/inputmask/dist/inputmask/inputmask.date.extensions.js")
	require("../vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js")
	require("../vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js")
	require("../vendors/nouislider/distribute/nouislider.js")
	require("../vendors/owl.carousel/dist/owl.carousel.js")
	require("../vendors/autosize/dist/autosize.js")
	require("../vendors/clipboard/dist/clipboard.min.js")
	require("../vendors/ion-rangeslider/js/ion.rangeSlider.js")
	require("../vendors/dropzone/dist/dropzone.js")
	require("../vendors/summernote/dist/summernote.js")
	require("../vendors/markdown/lib/markdown.js")
	require("../vendors/bootstrap-markdown/js/bootstrap-markdown.js")
	require("../vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js")
	require("../vendors/jquery-validation/dist/jquery.validate.js")
	require("../vendors/jquery-validation/dist/additional-methods.js")
	require("../vendors/js/framework/components/plugins/forms/jquery-validation.init.js")
	require("../vendors/bootstrap-notify/bootstrap-notify.min.js")
	require("../vendors/js/framework/components/plugins/base/bootstrap-notify.init.js")
	require("../vendors/toastr/build/toastr.min.js")
	require("../vendors/jstree/dist/jstree.js")
	require("../vendors/raphael/raphael.js")
	require("../vendors/morris.js/morris.js")
	require("../vendors/chartist/dist/chartist.js")
	require("../vendors/chart.js/dist/Chart.bundle.js")
	require("../vendors/js/framework/components/plugins/charts/chart.init.js")
	require("../vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js")
	require("../vendors/vendors/jquery-idletimer/idle-timer.min.js")
	require("../vendors/waypoints/lib/jquery.waypoints.js")
	require("../vendors/counterup/jquery.counterup.js")
	require("../vendors/es6-promise-polyfill/promise.min.js")
	window.swal = require("../vendors/sweetalert2/dist/sweetalert2.min.js")
	require("../vendors/element-ui/js/rate.js")
	require("../vendors/element-ui/js/icon.js")
	require("../vendors/slick/slick/slick.min.js")
	require("../vendors/animate.css/animate.min.css")
	//require("../vendors/js/framework/components/plugins/base/sweetalert2.init.js")

	require("./vendor/metronic/fullcalendar.bundle.js")
	require("./vendor/metronic/dashboard.js")

} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
