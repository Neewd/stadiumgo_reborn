
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');

import { Rate } from 'element-ui';
import { InputNumber } from 'element-ui';

// Passport Components
window.Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

window.Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

window.Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

window.Vue.use(Rate);
window.Vue.use(InputNumber);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./components/bootstrap');

import store from './store'

const app = new Vue({
    el: '#app',
    store
});
