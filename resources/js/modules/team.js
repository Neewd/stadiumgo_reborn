import axios from 'axios'
import _ from 'lodash'

const teamModule = {

	state: { 
		teams : []
	},	

	mutations: { 
		FETCH_TEAM(state, teams) {
	    	state.teams = teams;
	    }
	},

	getters: { 
		teams : state => state.teams
	},

	actions: { 
		fetchTeams({ commit }) {
			let url = '/api/v1/team'
			return axios.get(url)
	            .then(response => {
	            	commit('FETCH_TEAM', response.data)
	            })
	            .catch(error => {
				    console.log(error)
				});
		}
	 }

}

export default teamModule