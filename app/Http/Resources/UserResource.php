<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'id_team_supported' => $this->id_team_supported,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'gender' => $this->gender,
            'birthdate' => $this->birthdate,
            'email' => $this->email,
            'phone' => $this->phone,
            'description' => $this->description,
            'vehicle' => $this->vehicle,
            'avatar_path' => $this->avatar_path,
            'team' => new TeamResource($this->team),
            'date' => $this->created_at->diffForHumans(),
            'average_note' => $this->average_note
        ];
    }
}
