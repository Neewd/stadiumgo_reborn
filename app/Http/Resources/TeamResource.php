<?php

namespace App\Http\Resources;

use App\Team;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'sport' => Team::find($this->id)->sport->name,
            'logo' => $this->logo,
            'date' => $this->created_at->diffForHumans(),
            'pays' => $this->pays
        ];
    }
}
