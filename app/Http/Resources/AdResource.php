<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Date\Date;
use App\Http\Resources\MatchResource;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Log;

class AdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        Date::setLocale('fr');
        return [
            'id' => $this->id,
            'city_start' => $this->city_start,
            'date_depart' => Date::parse($this->date_depart)->format('d/m/Y H:i'),
            'date_depart_human' => ucwords(Date::parse($this->date_depart)->format('l j F Y H:i')),
            'distance' => $this->distance,
            'total_nb_places' => $this->total_nb_places,
            'remaining_places' => $this->remaining_places,
            'unit_price' => $this->unit_price,
            'distance' => $this->distance,
            'going_coming' => $this->going_coming,
            'duration' => $this->duration,
            'date_return' => Date::parse($this->date_return)->format('d/m/Y H:i'),
            'return_after_match' => $this->return_after_match,
            'created_at' => $this->created_at->diffForHumans(),
            'updated_at' => $this->updated_at->diffForHumans(),
            'match' => new MatchResource($this->match), 
            'user' => new UserResource($this->user)
        ];
    }
}
