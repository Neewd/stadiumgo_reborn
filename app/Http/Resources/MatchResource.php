<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\TeamResource;
use App\Http\Resources\CompetitionResource;
use App\Http\Resources\StadiumResource;
use App\Competition;
use App\Team;
use Carbon\Carbon;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\Log;

class MatchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        Date::setLocale('fr');
        return [
            'id' => $this->id,
            'competition' => new CompetitionResource($this->competition),
            'homeTeam' => new TeamResource($this->homeTeam),
            'outsideTeam' => new TeamResource($this->outsideTeam),
            'stade' => new StadiumResource($this->stadium),
            'journee' => $this->journee,
            'archived' => $this->archived,
            'highlighted' => $this->highlighted,
            'date_humans' => ucwords(Date::parse($this->date)->format('l j F Y H:i')),
            'date' => Date::parse($this->date)->format('d/m/Y'),
            'heure' => Date::parse($this->date)->format('H:i')
        ];
    }
}
