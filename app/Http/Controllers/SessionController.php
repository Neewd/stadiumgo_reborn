<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Session;

class SessionController extends Controller
{
    
	public function getSession(Request $request, $key) {
		if (session()->has($key)) {
			$message = session()->get($key);
			session()->forget($key);
			return response()->json($message);
		}  
		return "";	
	}

}
