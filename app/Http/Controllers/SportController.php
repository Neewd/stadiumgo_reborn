<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sport;
use App\Http\Resources\SportResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class SportController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $sports = Sport::get();
        $sportsTransformed = [];
        for ($i=0; $i < sizeof($sports); $i++) { 
            array_push($sportsTransformed, new SportResource($sports[$i]));
        }
        return response()->json($sportsTransformed);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        // Authorization
        $this->authorize('create-sport');

        // Validation
        $rules = array(
            'name' => 'required|min:3|unique:sports|max:50'
        );

        $messages = [
            'name.required' => 'Le nom du sport est nécessaire.',
            'name.min' => 'Le nom doit être plus grand que 3 caractères.',
            'name.unique' => "Ce nom de sport existe déjà.",
            'name.max' => "Ce nom de sport est beaucoup trop grand."
        ];

        $this->validate($request, $rules, $messages);

        $sport = Sport::create($request->all());

        return response()->json(new SportResource($sport));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sport  $sport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {           

        // Authorization
        $this->authorize('update-sport');

        // Validation
        $rules = array(
            'name' => 'required|min:3|unique:sports|max:50'
        );

        $messages = [
            'name.required' => 'Le nom du sport est nécessaire.',
            'name.min' => 'Le nom doit être plus grand que 3 caractères.',
            'name.unique' => "Ce nom de sport existe déjà.",
            'name.max' => "Ce nom de sport est beaucoup trop grand."
        ];

        $this->validate($request, $rules, $messages);

        $sport = Sport::findOrFail(Input::get('id'))->update([
                'name' => Input::get('name')
        ]);
            
        return response()->json($sport);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sport  $sport
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        // Authorization
        $this->authorize('delete-sport');

        $sport = Sport::findOrFail($id);
    
        return response()->json($sport->delete());
    }
}
