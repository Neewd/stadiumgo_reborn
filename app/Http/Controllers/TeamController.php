<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\Sport;
use App\Http\Resources\TeamResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;

// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Support\Facades\Storage;

class TeamController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $teams = Team::get();
        $teamsTransformed = [];
        for ($i=0; $i < sizeof($teams); $i++) { 
            array_push($teamsTransformed, new TeamResource($teams[$i]));
        }

        return response()->json($teamsTransformed);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $rules = array(
            'name'          => 'required|min:3|unique:teams',
            'stadium_id'    => 'required',
            'sport_id'      => 'required',
            'logo'          => 'required',
            'pays'          => 'required'
        );

        $messages = [
            'name.required' => "Le nom de l'équipe est nécessaire.",
            'name.min' => "Le nom doit être plus grand que 3 caractères.",
            'name.unique' => "Ce nom d'équipe existe déjà",
            'sport_id.required' => "Cette équipe ne joue à aucun sport ?",
            'pays.required' => "Evidemment ce club joue dans les eaux internationales ?",
            'logo.required' => 'Un club sans logo ?',
        ];

        $this->validate($request, $rules, $messages);

        // upload logo
        $logo = Image::make($request->logo);

        if ($logo->mime() == 'image/jpeg')
            $extension = '.jpg';
        elseif ($logo->mime() == 'image/png')
            $extension = '.png';

        $filename = $request->name . $extension;

        $logo->save(public_path('images/teams/' . $filename));
        
        $request['logo'] = $filename;

        $team = Team::create($request->all());

        return response()->json(new TeamResource($team));
        
    }

    public function storeOrUpdate(Request $request) {

        $metadata = $request->metadata;

        // On récupère l'identifant du sport
        $sport = Sport::where('name' , $metadata['sport'])
                    ->firstOrFail();

        $tableOfTeams = [];

        // Pour chaque journée
        foreach ($request->teams as $teamNumber => $team){
            try {
                $teamDB = Team::where('name', $team['name'])->firstOrFail();
                // Si elle est trouvée, on la modife seulement (logo / nom)
            } catch (\Exception $e) {
                // On créé la team
                $logo = Image::make($team['urlLogo']);
                $filename = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $team['name']))) . '.png';
                $logo->save(public_path('images/teams/' . $filename));

                $newTeam = new Team();
                $newTeam->name = $team['name'];
                $newTeam->sport_id = $sport->id;
                $newTeam->logo = $filename;
                $newTeam->pays = ucwords($team['pays']);

                Log::info($newTeam);

                $newTeam->save();

                Log::info("saved");

                array_push($tableOfTeams, $newTeam);
            }
        }
        return response()->json($tableOfTeams);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {           
        
        $team = Team::find($id);

        $rules = array(
            'name'          => 'required|min:3',
            'stadium_id'    => 'required',
            'sport_id'      => 'required',
            'pays'          => 'required'
        );

        $messages = [
            'name.required' => "Le nom de l'équipe est nécessaire.",
            'name.min' => "Le nom doit être plus grand que 3 caractères.",
            'sport_id.required' => "Cette équipe ne joue à aucun sport ?",
            'pays.required' => "Evidemment ce club joue dans les eaux internationales ?"
        ];

        $this->validate($request, $rules, $messages);

        // update logo
        if($request->logo){

            if($team->logo){
              unlink(public_path() . '/images/teams/' . $team->logo);
            }
  
            $updatePicture = Image::make($request->logo);

            if ($updatePicture->mime() == 'image/jpeg')
                $extension = '.jpg';
            elseif ($updatePicture->mime() == 'image/png')
                $extension = '.png';
  
            $filename = $request->name . $extension;
  
            $updatePicture->save(public_path('images/teams/' . $filename));

            $team->update(['logo' => $filename]);
        }
        
        $updatedTeam = Team::findOrFail(Input::get('id'))->update([
            'name' => Input::get('name'),
            'stadium_id' => Input::get('stadium_id'),
            'sport_id' => Input::get('sport_id'),
            'pays'      => Input::get('pays')
        ]);
            
        return response()->json($updatedTeam);      

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $team = Team::findOrFail($id);

        // delete associate logo
        if($team->logo){
            unlink(public_path() . '/images/teams/' . $team->logo);
        }
    
        return response()->json($team->delete());
    }
}
