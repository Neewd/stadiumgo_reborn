<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Match;
use App\Competition;
use App\Team;
use App\Stadium;
use App\Http\Resources\MatchResource;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matchs = Match::paginate(15);

        $matchs->getCollection()->transform(function ($match, $key) {
            return new MatchResource($match);
        });

        return response()->json($matchs);
    }

    /**
     * Store a new match or update the existing one if he's already in base.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeOrUpdate(Request $request)
    {   
        // On va récupérer les identifiants de compétitiions etc
        $metadata = $request['metadata'];
        $competition = Competition::where('slug' , $metadata['competition'])
                    ->where('pays', $metadata['pays'])
                    ->firstOrFail();

        $tableOfMatchs = [];

        // Pour chaque journée
        foreach ($request->matchs as $journeeNumber => $journeeMatch){

            // Et pour chaque match de la journée
            foreach ($journeeMatch as $matchNumber => $match){
                // Pour chaque match on va vérifier que le match existe et si tel est le cas on va simplement le modifier
                // Sinon, nous allons le créer.

                //Log::info($match);

                try {
                    $teamDBDomicile = Team::where('name', $match['equipeDomicile'])->firstOrFail();
                    $teamDBExterieur = Team::where('name', $match['equipeExterieur'])->firstOrFail();

                    $matchDB = Match::where('competition_id' , $competition->id)
                                    ->where('team_dom_id' , $teamDBDomicile->id)
                                    ->where('team_ext_id' , $teamDBExterieur->id)
                                    ->where('journee', $journeeNumber)
                                    ->firstOrFail();

                    //Log::info('Je passe ici car on a trouvé le match');

                    // Si on est ici c'est que le match à été trouvé ! Bien joué à nous !

                } catch (\Exception $e) {

                    // On va chercher l'id du stadium, pour voir s'il existe

                    try {

                        $stadiumFound = false;

                        // On va chercher le stade dans la base
                        $stadium = Stadium::where([
                            'name' => substr($match['stade']['name'], 0, strpos($match['stade']['name'], '(')), 
                            'address' => $match['stade']['adresseDuStade'],
                            'zipcode' => $match['stade']['zip_code'],
                            'city' => $match['stade']['city']
                        ])->firstOrFail();

                        $stadiumFound = true;

                    } catch (\Exception $e1) {  

                        // Si nous sommes ici c'est que le stade n'a pas été trouvé, et nous devons le créer
                        $newStade = new Stadium();
                        $newStade->name = substr($match['stade']['name'], 0, strpos($match['stade']['name'], '('));
                        $newStade->address = $match['stade']['adresseDuStade'];
                        $newStade->zipcode = $match['stade']['zip_code'];
                        $newStade->city = $match['stade']['city'];
                        $newStade->guide = "";

                        $newStade->save();

                    } finally {

                        // S'il n'est pas trouvé, go le créé 
                        $newMatch = new Match();
                        $newMatch->competition_id = $competition->id;
                        $newMatch->team_dom_id = $teamDBDomicile->id;
                        $newMatch->team_ext_id = $teamDBExterieur->id;
                        $newMatch->journee = $journeeNumber;
                        if ($stadiumFound) {
                            $newMatch->stadium_id = $stadium->id;
                        } else {
                            $newMatch->stadium_id = $newStade->id;
                        }
                        

                        $carbonDate = Carbon::createFromFormat('d.m.Y H:i', $match['date'] . ' ' . $match['heure']);
                        $newMatch->date = $carbonDate;

                        $newMatch->save();

                        array_push($tableOfMatchs, $newMatch);
                    }                    
                }
            }
        }
        return response()->json($tableOfMatchs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $rules = array(
            'competition_id' => 'required',
            'team_dom_id'   => 'required',
            'team_ext_id'    => 'required',
            'date'          => 'required|date_format:d/m/Y H:i|after:now'
        );

        $messages = [
            'competition_id.required' => "Un match à forcément une compétition non ?",
            'team_dom_id.required' => "Il faut qu'une équipe accueille l'autre non ?",
            'team_ext_id.required' => "Pour un match il faut deux équipes.",
            'date.required' => 'Et comment on fait sans date ?',
            'date.date' => "C'est pas une date ça !",
            'date.date_format' => "T'es sûr de connaître les dates ?",
            'date.after' => "Un match dans le passé ?"
        ];

        $this->validate($request, $rules, $messages);

        $carbonDate = Carbon::createFromFormat('d/m/Y H:i', Input::get('date'));
        $request->offsetSet('date',$carbonDate);

        $match = Match::create($request->all());

        return response()->json(new MatchResource($match));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'competition_id' => 'required',
            'team_dom_id'   => 'required',
            'team_ext_id'    => 'required',
            'date'          => 'required|date_format:d/m/Y H:i|after:now'
        );

        $messages = [
            'competition_id.required' => "Un match à forcément une compétition non ?",
            'team_dom_id.required' => "Il faut qu'une équipe accueille l'autre non ?",
            'team_ext_id.required' => "Pour un match il faut deux équipes.",
            'date.required' => 'Et comment on fait sans date ?',
            'date.date' => "C'est pas une date ça !",
            'date.date_format' => "T'es sûr de connaître les dates ?",
            'date.after' => "Un match dans le passé ?"
        ];

        $this->validate($request, $rules, $messages);

        $carbonDate = Carbon::createFromFormat('d/m/Y H:i', Input::get('date'));
        $request->offsetSet('date',$carbonDate);
        
        $match = Match::findOrFail(Input::get('id'))->update([
            'competition_id' => Input::get('competition_id'),
            'team_dom_id' => Input::get('team_dom_id'),
            'team_ext_id'   => Input::get('team_ext_id'),
            'date'   => Input::get('date'),
            'archived' => Input::get('archived'),
            'highlighted' => Input::get('highlighted')
        ]);
            
        return response()->json($match); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $match = Match::findOrFail($id);
    
        return response()->json($match->delete());
    }

    /**
     * Retrieve the forward (in the future) matchs for a team in input.
     *
     * @param  int  $id of the team
     * @return \Illuminate\Http\Response
     */
    public function getTeamMatchs($id) 
    {   
        $matchs = Match::where('archived' , 0)
            ->where(function($query) use($id) {
                $query->where('team_dom_id', $id)
                      ->orWhere('team_ext_id' , $id);

            })->get();


        $matchs->transform(function ($match, $key) {
            return new MatchResource($match);
        });

        return response()->json($matchs);
    }
}
