<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stadium;
use App\Http\Resources\StadiumResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;

class StadiumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stadiums = Stadium::get();
        $stadiumsTransformed = [];
        for ($i=0; $i < sizeof($stadiums); $i++) { 
            array_push($stadiumsTransformed, new StadiumResource($stadiums[$i]));
        }
        return response()->json($stadiumsTransformed);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|min:5|unique:stadiums',
            'address' => 'required',
            'city' => 'required'
        );

        $messages = [
            'name.required' => 'Le nom du stade est nécessaire.',
            'name.min' => 'Le nom doit être plus grand que 5 caractères.',
            'name.unique' => "Ce nom de stade existe déjà",
            'address.required' => "L'adresse du stade est nécessaire.",
            'city.required' => 'La ville du stade est nécessaire.',
        ];

        $this->validate($request, $rules, $messages);

        $stadium = Stadium::create($request->all());

        return response()->json(new StadiumResource($stadium));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required|min:5',
            'address' => 'required',
            'city' => 'required'
        );

        $messages = [
            'name.required' => 'Le nom du stade est nécessaire.',
            'name.min' => 'Le nom doit être plus grand que 5 caractères.',
            'address.required' => "L'adresse du stade est nécessaire.",
            'city.required' => 'La ville du stade est nécessaire.',
        ];

        $this->validate($request, $rules, $messages);

        $stadium = Stadium::findOrFail(Input::get('id'))->update([
            'name' => Input::get('name'),
            'address' => Input::get('address'),
            'zipcode' => Input::get('zipcode'),
            'city' => Input::get('city')
        ]);

        return response()->json($stadium);  
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $stadium = Stadium::findOrFail($id);
    
        return response()->json($stadium->delete());
    }
}
