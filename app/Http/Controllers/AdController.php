<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Http\Resources\AdResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules = array(
            'city_start' => 'required|min:3',
            'lat_city_start' => 'required',
            'lng_city_start' => 'required',
            'date_depart' => 'required|date',
            'total_nb_places' => 'required',
            'remaining_places' => 'required',
            'unit_price' => 'required',
            'distance' => 'required',
            'duration' => 'required',
            'going_coming' => 'required',
            'return_after_match' => 'required',
            'date_return' => 'required|date',
            'match_id' => 'required',
            'user_id' => 'required',
        );

        $messages = [
            'city_start.required' => 'Une ville de départ ne serait-elle pas de trop ?',
            'date_depart.required' => 'Une date de départ est nécessaire',
            'date_depart.date' => "C'est pas une date ça",
            'total_nb_places.required' => 'Un nombre de places est nécessaire',
            'remaining_places.required' => 'Il reste combien de place dans votre trajet ?',
            'unit_price.required' => "Genre c'est gratuit ?",
            'distance.required' => "Le trajet à besoin d'une distance",
            'duration.required' => "Le trajet à besoin d'une durée",
            'going_coming.required' => 'Le trajet est-il aller retour ?',
            'return_after_match.required' => "Le retour s'effectuera juste après le match ?",
            'date_return.required' => 'La date de retour est nécessaire',
            'date_return.date' => "Ce n'est pas une date ça",
            'match_id.required' => "On a besoin dun match pour une annonce non ?",
            'user_id.required' => 'Personne ne va conduire ?',
        ];

        $this->validate($request, $rules, $messages);

        $ad = Ad::create($request->all());

        return response()->json(new AdResource($ad));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
        return view('ad.proposer-trajet');
    }

    /**
     * Display the form who display the ads created by the users related with the search oprions.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAnnonceRelatedWithSearch() {

        $ads = [];

        // Log::info("je passe");
        // Log::info(Input::get('club-choisi'));

        if (Input::get('club-choisi') != '' && Input::get('villeDepartRecherche') != '') {

            $adsUntransformed = Ad::whereHas('match' , function($query) {
                $query->where('team_dom_id', Input::get('id-club-choisi'))
                      ->orWhere('team_ext_id', Input::get('id-club-choisi'));
            })->get();

            $adsUntransformed->transform(function ($item, $key) {
                $kilometers = AdController::getDistanceBetweenPoints(Input::get('lat_ville'), Input::get('lng_ville'), $item['lat_city_start'], $item['lng_city_start']);
                $item['distance'] = round($kilometers,0);
                return $item;
            });

        } else {
            $adsUntransformed = Ad::get();
            $adsUntransformed->transform(function ($item, $key) {
                $kilometers = 0;
                $item['distance'] = $kilometers;
                return $item;
            });
        }

        for ($i=0; $i < sizeof($adsUntransformed); $i++) { 
            array_push($ads, new AdResource($adsUntransformed[$i]));
        }

        $ville = new \stdClass();
        $ville->name = Input::get('villeDepartRecherche');
        $ville->lat = Input::get('lat_ville');
        $ville->lng = Input::get('lng_ville');
        $club = new \stdClass();
        $club->name = Input::get('club-choisi');
        $club->id = Input::get('id-club-choisi');

        return view('ad.display-annonces-by-search', compact('ads', 'ville', 'club'));
    }

    /**
     * Display the ads thanks to the ID in input
     *
     * @param  String $id
     * @return \Illuminate\Http\Response
     */
    public function showAnnonceById($id) {

        $adUntransformed = Ad::where('id', $id)->firstOrFail();

        $ad = new AdResource($adUntransformed);

        return view('ad.display-annonce', compact('ad'));
    }

    public function getAdForSearch($club, $villeLat, $villeLng) {

        $ads = [];

        $adsUntransformed = Ad::whereHas('match' , function($query) use ($club) {
                $query->where('team_dom_id', $club)
                      ->orWhere('team_ext_id', $club);
        })->get();

        $adsUntransformed->transform(function ($item, $key) use ($villeLat, $villeLng) {
            $kilometers = AdController::getDistanceBetweenPoints($villeLat, $villeLng, $item['lat_city_start'], $item['lng_city_start']);
            $item['distance'] = round($kilometers,0);
            return $item;
        });

        for ($i=0; $i < sizeof($adsUntransformed); $i++) { 
            array_push($ads, new AdResource($adsUntransformed[$i]));
        }

        return response()->json($ads);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        //
    }

    /**
     * Retourne la distance en metre ou kilometre (si $unit = 'k') entre deux latitude et longitude fournit
     */
    private static function getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
        $miles = acos($miles);
        $miles = rad2deg($miles);
        $miles = $miles * 60 * 1.1515;
        $feet = $miles * 5280;
        $yards = $feet / 3;
        $kilometers = $miles * 1.609344;
        $meters = $kilometers * 1000;
        return $kilometers; 
    }
}
