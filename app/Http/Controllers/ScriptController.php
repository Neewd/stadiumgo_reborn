<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScriptConfig;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;

class ScriptController extends Controller
{
	/**
     * Retrieve the config of the script, which competition we wants the matchs retrieved etc.
     *
     * @return \Illuminate\Http\Response
     */
    public function getScriptConfig() {
    	Log::info('pouet');
    	$scriptConfig = ScriptConfig::get();
    	return $scriptConfig;
    }
}
