<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Competition;
use App\Http\Resources\CompetitionResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;

class CompetitionController extends Controller
{   

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $competitions = Competition::get();

        $competitionsTransformed = [];
        for ($i=0; $i < sizeof($competitions); $i++) { 
            array_push($competitionsTransformed, new CompetitionResource($competitions[$i]));
        }

        return response()->json($competitionsTransformed);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $rules = array(
            'name' => 'required|min:3|unique:competitions|max:50',
            'slug' => 'required',
            'pays' => 'required'
        );

        $messages = [
            'name.required' => 'Le nom de la compétition est nécessaire.',
            'name.min' => 'Le nom doit être plus grand que 3 caractères.',
            'name.unique' => 'Cette compétition existe déjà.',
            'name.max' => 'Le nom ne doit pas excéder 50 caractères.',
            'slug.required' => "Le slug est nécessaire.",
            'pays.required' => 'Le pays de la compétition est nécessaire.',
        ];

        $this->validate($request, $rules, $messages);

        $competition = Competition::create($request->all());

        return response()->json(new CompetitionResource($competition));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Competition  $competition
     * @return \Illuminate\Http\Response
     */
    public function show(Competition $competition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Competition  $competition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {           

        $rules = array(
            'name' => 'required|min:3|unique:competitions|max:50',
            'slug' => 'required',
            'pays' => 'required'
        );

        $messages = [
            'name.required' => 'Le nom de la compétition est nécessaire.',
            'name.min' => 'Le nom doit être plus grand que 3 caractères.',
            'name.unique' => 'Cette compétition existe déjà.',
            'name.max' => 'Le nom ne doit pas excéder 50 caractères.',
            'slug.required' => "Le slug est nécessaire.",
            'pays.required' => 'Le pays de la compétition est nécessaire.',
        ];

        $this->validate($request, $rules, $messages);

        $competition = Competition::findOrFail(Input::get('id'))->update([
            'name' => Input::get('name'),
            'slug' => Input::get('slug'),
            'pays' => Input::get('pays')
        ]);
            
        return response()->json($competition);
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Competition  $competition
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $compet = Competition::findOrFail($id);
    
        return response()->json($compet->delete());
    }
}
