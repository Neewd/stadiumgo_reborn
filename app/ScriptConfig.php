<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScriptConfig extends Model
{
    protected $fillable = ['competition', 'sport', 'pays'];

    protected $table = 'params_scripts';
}
