<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name', 'logo', 'stadium_id', 'sport_id'];

    public function sport(){
        return $this->belongsTo('App\Sport');
    }

    public function stadium(){
        return $this->belongsTo('App\Stadium');
    }
}
