<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable = ['competition_id','team_dom_id','team_ext_id','date', 'journee', 'archived', 'highlighted'];

    protected $table = 'matchs';

    protected $casts = [
        'archived' => 'boolean',
        'highlighted' => 'boolean'
    ];

    public function competition() {
        return $this->belongsTo('App\Competition');
    }

    public function homeTeam() {
        return $this->belongsTo('App\Team','team_dom_id');
    }

    public function outsideTeam() {
        return $this->belongsTo('App\Team', 'team_ext_id');
    }

    public function stadium() {
        return $this->belongsTo('App\Stadium', 'stadium_id');
    }

    public function ads() {
        return $this->hasMany('App\Ad');
    }
}
