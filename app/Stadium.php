<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stadium extends Model
{
    protected $fillable = ['name', 'address', 'city', 'zipcode'];

    protected $table = 'stadiums';

    public function teams()
    {
        return $this->hasMany('App\Team');
    }

}
