<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $fillable = ['city_start', 'lat_city_start','lng_city_start','date_depart', 'total_nb_places' , 'remaining_places' , 'unit_price', 'distance' , 'going_coming' , 'duration', 'date_return' , 'return_after_match' , 'match_id' , 'user_id'];

	protected $table = 'ads';

    protected $casts = [
        'going_coming' => 'boolean',
        'return_after_match' => 'boolean'
    ];

	public function match() {
        return $this->belongsTo('App\Match','match_id');
    }

	public function user() {
        return $this->belongsTo('App\User','user_id');
    }

}
