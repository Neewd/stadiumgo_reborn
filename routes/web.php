<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

// Authentication routes
Auth::routes();

// Protected routes
Route::group(['middleware' => 'auth'], function() {
	Route::get('/proposer-trajet', 'AdController@show')->name('proposer-trajet');
	Route::group(['prefix' =>  'dashboard'], function() {
		Route::get('/', 'DashboardController@show')->name('dashboard');
		Route::get('settings', 'SettingsController@show')->name('settings');
		Route::group(['prefix' =>  'settings'], function() {
			Route::get('/api', 'SettingsController@showApiSettings')->name('api')->middleware('admin');
		});	
	});
});

// Unprotected routes
Route::post('/recherche-annonce', 'AdController@showAnnonceRelatedWithSearch');
Route::get('/recherche-annonce', 'AdController@showAnnonceRelatedWithSearch');
Route::get('/annonce/{id}', 'AdController@showAnnonceById');