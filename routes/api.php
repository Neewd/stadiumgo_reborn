<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
* Route used by the script who retrieve the match
*/
Route::group(['prefix' => 'client',  'middleware' => 'client'], function() {
	Route::post('/matchs', 'MatchController@storeOrUpdate');
	Route::post('/teams', 'TeamController@storeOrUpdate');
	Route::get('/script-config', 'ScriptController@getScriptConfig');
});

/*
  Public API Routes
*/
Route::group(['prefix' => 'v1'], function() {
  Route::get('/team', 'TeamController@index');
  Route::get('/match/team/{id}' , 'MatchController@getTeamMatchs');
});

/*
  Authenticated API Routes.
*/
Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function(){

	//Route::resource('/competition', 'CompetitionController');
  	//Route::resource('/sport', 'SportController');
  	//Route::resource('/team', 'TeamController')->except(['index']);
  	//Route::resource('/user', 'UserController')->except(['index','store']);;
	//Route::get('/users', 'UserController@getUsers');
  	//Route::resource('/stadium', 'StadiumController');
  	//Route::resource('/match', 'MatchController');

  	Route::resource('/ad' , 'AdController');
    Route::get('/ad/search/{club}/{villeLat}/{villeLng}', 'AdController@getAdForSearch');

});
